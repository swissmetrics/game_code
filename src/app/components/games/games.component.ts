import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { GamesModel } from '../../models/games.model';

@Component({
    selector: 'app-comment',
    templateUrl: '../../templates/games/game-setup.html',
})

export class GamesComponent implements OnInit{

    public games: Array<GamesModel>;

    constructor(
        private gameService: GameService) {
    }

    ngOnInit() {

        let serviceApi = 'assets/jsons/games.json';

        // all available games
        this.gameService
            .getGames(serviceApi)
            .subscribe(ret => {
                this.games = ret;
            });
    }

}
