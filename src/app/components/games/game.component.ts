import { Component, OnInit, OnDestroy } from '@angular/core';
import { GameService } from '../../services/game.service';
import { ActivatedRoute } from '@angular/router';
import { GameSetupModel } from '../../models/gameSetup.model';

@Component({
    selector: 'app-comment',
    templateUrl: '../../templates/games/game-setup-detail.html',
})

export class GameComponent implements OnInit, OnDestroy{

    private sub: any;
    public modelAttr = {};
    public game: GameSetupModel;

    constructor(
        private route: ActivatedRoute,
        private gameService: GameService) {
            this.gameService.saved = false;
    }

    ngOnInit() {
        // params from url
        this.sub = this.route.params.subscribe( params => {
            let id = +params['id'];
            this.gameAction(id);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    // load game params
    gameAction(id:number){
        let serviceApi = `assets/jsons/games-${id}.json`;
        // game params
        this.gameService
            .getGameSetup(serviceApi)
            .subscribe(ret => {
                this.game = ret;
                for(let atr of this.game['params']){
                    this.modelAttr[atr.name] =  atr.value;
                }
            });
    }

    // save game configuration in DB
    saveGame(): void{

        let serviceApi = 'assets/jsons/setup-coin-acceptor.json';

        this.gameService
            .saveGameSetup(serviceApi, this.readParams())
            .subscribe(
                ret => {
                    this.gameService.saved = true;
                },
                error => {
                    this.gameService.error = true;
                }
            );
    }

    // read param from form
    readParams(){

        let params = this.modelAttr;
        console.log(params);

        return params;
    }

}
