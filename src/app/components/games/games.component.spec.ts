import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { GamesComponent } from './games.component';
import { RestService } from '../../services/rest.service';
import { GameService } from '../../services/game.service';
import { FormsModule } from '@angular/forms';
import { MdKeyboardService } from 'ngx-material-keyboard';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';

describe('Games component', () => {

    let fixture: ComponentFixture<GamesComponent>;
    let component: GamesComponent;

    beforeEach( async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpModule, FormsModule, HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [GamesComponent],
            providers: [RestService, GameService, MdKeyboardService, HttpClient, TranslateService],
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(GamesComponent);
            component = fixture.componentInstance;
            component.ngOnInit();
        });
    }));

    it('should be displayed game buttons', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(fixture.debugElement.nativeElement
                .querySelectorAll('article.game-setup .buttons-group button').length).toBeGreaterThan(0);
        });
    }));

});
