import { Component, DoCheck } from '@angular/core';
import { KeyboardService } from '../services/keyboard.service';

@Component({
    selector: 'numeric-keyboard',
    templateUrl: '../templates/numeric-keyboard.html'
})

export class KeyboardComponent implements DoCheck {

    public showKeyboard: boolean = false;

    constructor(private keyboardService: KeyboardService) {
    }

    pressKey(key: string) {
        this.keyboardService.currentValue += key;
        this.insert(this.keyboardService.currentValue);
    }

    // remove last digit
    clear() {
        const strValue = this.keyboardService.eventTarget.value.toString();
        const cutValue = strValue.substring(0, strValue.length - 1)
        this.insert(cutValue);
        this.keyboardService.currentValue = cutValue;
    }

    insert(value: string) {
        this.keyboardService.eventTarget.value = value ? parseInt(value) : 0;
        this.keyboardService.eventTarget.focus();
    }

    ngDoCheck() {
        this.showKeyboard = this.keyboardService.showKeyboard;
    }
}
