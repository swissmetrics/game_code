import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/printer.html'
})

// Printer test
export class TestPrinterComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public ready: boolean = false;
    public progress: boolean = false;
    private testName: string = 'printer';
    public testStatus: string;

    constructor(private testsService: TestsService) {
            this.testsService.setup(this.testName);
    }

    ngOnInit() {
        this.testMenuComponent.isActive = this.testName;
    }

    // execute test
    testit(){
        this.testMenuComponent.isProgress = this.testName;
        this.testsService.progress();
        this.progress = true;
        // fake priner
        setTimeout(() => {
            this.ready = true;
            this.progress = false;
        }, 2000);
    }

    // do it when test succeed
    success(){
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testMenuComponent.isSuccess = this.testName;
        this.ready = false;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
