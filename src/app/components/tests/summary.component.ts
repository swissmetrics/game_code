import { Component, OnInit, ViewChild } from '@angular/core';
import { Router }  from '@angular/router';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/summary.html'
})

export class SummaryTestsComponent implements OnInit {

    @ViewChild(TestMenuComponent)
    testMenuComponent: TestMenuComponent;

    // fake last test name
    private testName = '*';
    public passed: number;
    public failed: number;

    constructor(
        private router: Router,
        private testsService: TestsService) {
    }

    ngOnInit() {

        this.passed = this.testsService.passed.length;
        this.failed = this.testsService.failed.length;

        if(this.passed == 0){
            this.testMenuComponent.isActive = 'none';
        }

        if(this.failed > 0){
            this.testMenuComponent.isActive = this.testsService.failed[0];
            this.testMenuComponent.isFail = this.testsService.failed[0];
        }
    }

    runAgain() {
        this.testMenuComponent.progressBarClass = 'begin';
        this.testsService.clearStopNext();
        this.router.navigate(['/tests/testing', 'test-1']);
    }

    print() {
        this.testsService.clearStopNext();
        this.router.navigate(['/tests']);
    }
}
