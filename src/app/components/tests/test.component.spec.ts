import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestComponent } from './test.component';
import { TestsService } from '../../services/tests.service';
import { RestService } from '../../services/rest.service';
import { HttpModule } from '@angular/http';
import { TestMenuComponent }  from './menu.component';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';

describe('TestComponent', () => {

  let fixture: ComponentFixture<TestComponent>;
  let component: TestComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpModule, HttpClientModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                    deps: [HttpClient]
                }
            })
        ],
        declarations: [TestComponent, TestMenuComponent],
        providers: [RestService, TestsService, HttpClient, TranslateService],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(TestComponent);
      component = fixture.componentInstance;
      component.ngOnInit();
    });

  }));

  it('should render step bar', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelectorAll('ul.checkout-bar li').length).toBeGreaterThan(2);
    });
  }));

});
