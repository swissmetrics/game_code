import { Component, OnInit, ViewChild } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/testSpin.html'
})

// Spin button test
export class TestSpinComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public ready: boolean = false;
    private testName: string = 'spin';
    private moveToNext: number;
    private channelServer = null;
    private actionName:string = 'insert_coin';
    public testStatus: string;

    constructor(
        private testsService: TestsService,
        private websocket: WebsocketService) {
    }

    ngOnInit() {

        this.testsService.setup(this.testName);
        this.testMenuComponent.isActive = this.testName;
        this.testMenuComponent.progressBarClass = "started";

        let socket = this.websocket.connect();

        socket.onError( ev => {
            console.log("WS: Connection Error", ev);
            return false;
        })

        this.channelServer = this.websocket.channel(socket);

        this.channelServer.join()
            .receive("ok", () => this.ready = true)
            .receive("error", () => { this.fail(); return; })

        // response from server - after press the button
        this.channelServer.on("action", msg => {
            if(msg.name == this.actionName){
                this.success();
                socket.disconnect();
            }
            else{
                this.fail();
            }
        });
    }

    // execute test
    testit(){

        this.testsService.progress();

        let payload = {
            name: this.actionName,
            payload: 3,
            timestamp: Date.now(),
        };

        // send data to the server
        this.channelServer.push("action", payload);
    }

    // do it when test succeed
    success(){
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testMenuComponent.isSuccess = this.testName;
        this.ready = false;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
        this.testMenuComponent.progressBarClass = "";
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
