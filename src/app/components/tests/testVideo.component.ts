import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/video.html'
})

export class TestVideoComponent implements OnInit {

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public tapped: boolean = false;
    private testName: string = 'video';
    public testStatus: string;

    constructor(
        private testsService: TestsService) {
            this.testsService.setup(this.testName);
    }

    ngOnInit() {
        this.testMenuComponent.isActive = this.testName;
        this.testMenuComponent.isProgress = this.testName;
    }

    // do it when test succeed
    success() {
        this.testsService.progress();
        this.testMenuComponent.isSuccess = this.testName;
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.tapped = true;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail() {
        this.tapped = true;
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
