import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { TestSpinComponent } from './testSpin.component';
import { TestsService } from '../../services/tests.service';
import { RestService } from '../../services/rest.service';
import { WebsocketService } from '../../services/websocket.service';
import { TestMenuComponent }  from './menu.component';
import { MaterialModule } from '@angular/material';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';

describe('Spin Button Test', () => {

    let fixture: ComponentFixture<TestSpinComponent>;
    let component: TestSpinComponent;

    beforeEach( async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpModule, HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                        deps: [HttpClient]
                    }
                })
            ],
            declarations: [TestSpinComponent, TestMenuComponent],
            providers: [WebsocketService, RestService, TestsService, HttpClient, TranslateService],
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(TestSpinComponent);
            component = fixture.componentInstance;
        });
    }));

    it('should be displayed RUN button - connected to websocket', async(() => {

        let websocket = new WebsocketService();
        let socket = websocket.connect();
        let channelServer = websocket.channel(socket, 'cabinet:server_events');

        channelServer.join()
            .receive("ok", () => {
                this.ready = true;
                expect(fixture.debugElement.nativeElement.querySelectorAll('.run-button button').length).toEqual(1);
            });
    }));

});
