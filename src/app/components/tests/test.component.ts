import { Component, OnInit, ViewChild } from '@angular/core';
import { Router }  from '@angular/router';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/main.html'
})

export class TestComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string = 'Test Page';
    public testNames = [];
    public setActive = '';

    constructor(
        private testsService: TestsService,
        private router: Router) {
            this.testsService.clearStopNext();
    }

    ngOnInit() {
        this.testMenuComponent.progressBarClass = 'begin';
    }

    // run all tests
    runAllTest(){
        this.router.navigate(['/tests/testing', 'test-1']);
    }

}
