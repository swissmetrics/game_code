import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Input, ElementRef } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';
import { WindowRef } from '../../services/window.service';

@Component({
    templateUrl: '../../templates/tests/audio.html',
    providers: [ WindowRef ]
})

export class TestAudioComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild(TestMenuComponent) testMenuComponent: TestMenuComponent;

    public title: string;
    public leftValue: number = 70;
    public rightValue: number = 70;
    public finished: boolean = false;
    public testLeftChannel: boolean = true;
    public testRightChannel: boolean = false;
    public testStatus: string;
    private testName = 'audio';
    private gainLeft: any;
    private gainRight: any;
    private audioCtx: any;
    private steepVolume:number = 10;
    private musicSource: Array<string> = ['assets/audio/test-1.mp3', 'assets/audio/test-2.mp3'];
    private player:any;

    constructor(
        private wr: WindowRef,
        private testsService: TestsService) {
            this.testsService.setup(this.testName);
    }

    ngOnInit() {
        this.testMenuComponent.isActive = this.testName;
        this.testit();
    }

    ngAfterViewInit() {
        this.play(this.musicSource[0]);
        this.setUpPlayer();
        this.setRightChannel(0);
    }

    ngOnDestroy() {
        if (this.audioCtx.state !== 'closed') {
            this.stop();
        }
    }

    // execute test
    testit() {
        this.testMenuComponent.isProgress = this.testName;
        this.testsService.progress();
    }

    // do it when test succeed
    success() {
        this.stop();
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testMenuComponent.isSuccess = this.testName;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail() {
        this.stop();
        this.finished = true;
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }

    // play audio
    play(src:string): void {
        this.player = new Audio(src);
        this.player.play();
    }

    // stop playing
    stop(): void {
        if (this.audioCtx) {
            this.audioCtx.close();
        }
    }

    // play right channels
    playRight(): void {
        this.testRightChannel = true;
        this.testLeftChannel = false;
        this.stop();
        this.play(this.musicSource[0]);
        this.setUpPlayer();
        this.setLeftChannel(0);
        this.setRightChannel(this.rightValue);
    }

    // play both channels
    playBoth(): void {
        this.testRightChannel = true;
        this.testLeftChannel = true;
        this.stop();
        this.play(this.musicSource[1]);
        this.setUpPlayer();
        this.setLeftChannel(this.leftValue);
        this.setRightChannel(this.rightValue);
    }

    // set up audio player
    setUpPlayer() {
        this.audioCtx = new (this.wr.nativeWindow.AudioContext || this.wr.nativeWindow.webkitAudioContext)();
        let source = this.audioCtx.createMediaElementSource(this.player);
        let splitter = this.audioCtx.createChannelSplitter(2);
        let merger = this.audioCtx.createChannelMerger(2);
        this.gainLeft = this.audioCtx.createGain();
        this.gainRight = this.audioCtx.createGain();
        // filter graph
        source.connect(splitter);
        splitter.connect(this.gainLeft, 0);
        splitter.connect(this.gainRight, 0);
        this.gainLeft.connect(merger, 0, 0);
        this.gainRight.connect(merger, 0, 1);
        merger.connect(this.audioCtx.destination);

        this.setLeftChannel(this.leftValue);
        this.setRightChannel(this.rightValue);
    }

    // set left channel volume
    setLeftChannel(value:number) {
        this.gainLeft.gain.value = value / 100;
    }

    // set right channel volume
    setRightChannel(value:number) {
        this.gainRight.gain.value = value / 100;
    }

    // increase left channel volume by clicking in button
    upLeftVolume():void {
        this.leftValue += this.steepVolume;
        if(this.leftValue >= 100){
            this.leftValue = 100;
        }
        this.setLeftChannel(this.leftValue);
    }

    // increase right channel volume by clicking in button
    upRightVolume():void {
        this.rightValue += this.steepVolume;
        if(this.rightValue >= 100) {
            this.rightValue = 100;
        }
        this.setRightChannel(this.rightValue);
    }

    // decrease left channel volume by clicking in button
    downLeftVolume() {
        this.leftValue -= this.steepVolume;
        if(this.leftValue <= 0){
            this.leftValue = 0;
        }
        this.setLeftChannel(this.leftValue);
    }

    // decrease right channel volume by clicking in button
    downRightVolume() {
        this.rightValue -= this.steepVolume;
        if (this.rightValue <= 0) {
            this.rightValue = 0;
        }
        this.setRightChannel(this.rightValue);
    }
}
