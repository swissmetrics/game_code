import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestVideoComponent } from './testVideo.component';
import { TestsService } from '../../services/tests.service';
import { RestService } from '../../services/rest.service';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { TestMenuComponent }  from './menu.component';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';

describe('Video Screen Test', () => {

  let fixture: ComponentFixture<TestVideoComponent>;
  let component: TestVideoComponent;

  beforeEach( async(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpModule, MaterialModule, HttpClientModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                    deps: [HttpClient]
                }
            })
        ],
        declarations: [TestVideoComponent, TestMenuComponent],
        providers: [TestsService, RestService, HttpClient, TranslateService],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(TestVideoComponent);
      component = fixture.componentInstance;
      component.ngOnInit();
    });
  }));

  it('should display video test screen', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
        expect(fixture.debugElement.nativeElement.querySelectorAll('section.video figure').length).toEqual(1);
    });
  }));

});
