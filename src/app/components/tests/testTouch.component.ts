import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';
import { TestTouchModel } from '../../models/testTouch.model';

@Component({
    templateUrl: '../../templates/tests/touchscreen.html'
})

export class TestTouchComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public coordinates: Array<TestTouchModel>;
    public successIcon: Array<boolean> = [];
    public failIcon: Array<boolean> = [];
    private testName: string = 'touch';
    private successClick: number = 0;
    public testStatus: string;

    constructor(
        private testsService: TestsService) {
            this.testsService.setup(this.testName);

            let serviceApi = 'assets/jsons/test-touch.json';
            this.testsService
                .getTouchPoints(serviceApi)
                .subscribe(ret => {
                    this.coordinates = ret;
                    for(let c in this.coordinates){
                        this.successIcon[c] = false;
                        this.failIcon[c] = false;
                    }
                });
    }

    ngOnInit() {
        this.testMenuComponent.isActive = this.testName;
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
    }

    // execute test - click in the single button
    testit(event, index){

        this.testsService.progress();
        this.testMenuComponent.isProgress = this.testName;

        event.target.classList.add('success');
        this.successIcon[index] = true;
        this.successClick += 1;

        if(this.successIcon.length === this.successClick){
            this.success();
        }

    }

    // do it when test succeed
    success(){
        this.testMenuComponent.isSuccess = this.testName;
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
