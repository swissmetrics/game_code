import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/self.html'
})

// Hopper test
export class TestSelfComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public ready: boolean = false;
    public progress: boolean = false;
    public tests: Array<string> = ['test_1', 'test_2', 'test_3', 'test_4', 'test_5'];
    public status: Array<string> = [];
    private testName: string = 'self';
    public testStatus: string;

    constructor(private testsService: TestsService) {
            this.testsService.setup(this.testName);
            for(let t in this.tests){
                this.status[t] = 'progress';
            }
    }

    ngOnInit() {
        this.testMenuComponent.isActive = this.testName;
    }

    // execute test
    testit(){
        this.testMenuComponent.isProgress = this.testName;
        this.testsService.progress();
        this.ready = true;
        //fake tests
        for(let t in this.tests){
            ((d: number) => {
                setTimeout(() => {
                    this.status[d] = 'success';
                    if(d == this.tests.length - 1){
                        setTimeout(() => {
                            this.success();
                        }, 2000);
                    }
                }, d * 1000);
            })(parseInt(t));
        }
    }

    // do it when test succeed
    success(){
        this.testMenuComponent.isSuccess = this.testName;
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.ready = false;
        this.testsService.success();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
