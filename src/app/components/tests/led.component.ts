import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';

@Component({
    templateUrl: '../../templates/tests/led.html'
})

export class TestLedComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public channels: Array<string>;
    public status: Array<string> = [];
    public order: number;
    public testStatus: string;
    public testName: string = 'led';

    constructor(
        private testsService: TestsService) {
            this.channels = ['left', 'top', 'right'];
    }

    ngOnInit() {
        this.testsService.setup(this.testName);
        this.testMenuComponent.isActive = this.testName;
        this.testit();
    }

    // execute test
    testit(){
        this.testsService.progress();
        this.testMenuComponent.isProgress = this.testName;
        this.order = 0;
        this.flashing('inprogress');
    }

    //fake test
    flashing(status: string = 'fail'){
        this.status[this.order] = status;
        if(status == 'fail'){
            this.fail();
        }
        else if(status == 'success'){
            if(this.order == this.channels.length - 1){
                this.success();
            } else {
                this.order++;
                this.status[this.order] = 'inprogress';
            }
        }
    }

    // do it when test succeed
    success(){
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testMenuComponent.isSuccess = this.testName;
        this.testsService.success();
        this.testMenuComponent.isSuccess = this.testName;
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
