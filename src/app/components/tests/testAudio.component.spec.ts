import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestAudioComponent } from './testAudio.component';
import { TestsService } from '../../services/tests.service';
import { RestService } from '../../services/rest.service';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { TestMenuComponent }  from './menu.component';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';
import 'hammerjs';

describe('Audio Test', () => {

  let fixture: ComponentFixture<TestAudioComponent>;
  let component: TestAudioComponent;

  beforeEach( async(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpModule, MaterialModule, HttpClientModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                    deps: [HttpClient]
                }
            })
        ],
        declarations: [TestAudioComponent, TestMenuComponent],
        providers: [TestsService, RestService, HttpClient, TranslateService],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(TestAudioComponent);
      component = fixture.componentInstance;
      component.ngOnInit();
      component.testRightChannel = true;
    });
  }));

  it('should display the right channel', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
        expect(fixture.debugElement.nativeElement.querySelectorAll('section.audio .right-channel').length).toEqual(1);
    });
  }));

});
