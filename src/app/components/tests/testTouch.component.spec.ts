import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestTouchComponent } from './testTouch.component';
import { TestsService } from '../../services/tests.service';
import { RestService } from '../../services/rest.service';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { TestMenuComponent }  from './menu.component';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';

describe('TouchScreen Test', () => {

  let fixture: ComponentFixture<TestTouchComponent>;
  let component: TestTouchComponent;

  beforeEach( async(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpModule, MaterialModule, HttpClientModule,
            TranslateModule.forRoot({
                loader: {
                    provide: TranslateLoader,
                    useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                    deps: [HttpClient]
                }
            })
        ],
        declarations: [TestTouchComponent, TestMenuComponent],
        providers: [TestsService, RestService, HttpClient, TranslateService],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(TestTouchComponent);
      component = fixture.componentInstance;
      component.ngOnInit();
    });
  }));

  it('should count up buttons on the page', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
        expect(fixture.debugElement.nativeElement.querySelectorAll('section.touchscreen a.button').length).toBeGreaterThan(0);
    });
  }));

});
