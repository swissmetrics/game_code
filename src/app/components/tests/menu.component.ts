import { Component } from '@angular/core';
import { Router }  from '@angular/router';
import { TestsService } from '../../services/tests.service';
import { TestsModel } from '../../models/tests.model';

@Component({
    selector: 'step-bar',
    templateUrl: '../../templates/tests/steps.html',
    styleUrls: ['../../../assets/styles/step-bar.scss']
})

export class TestMenuComponent{

    public testNames: Array<TestsModel>;
    public isProgress: string;
    public isSuccess: string;
    public isFail: string;
    public isActive: string;
    public nextTestName:string;
    public passed: Array<string> = [];
    public failed: Array<string> = [];
    public progressBarClass: string;

    constructor(private testsService: TestsService, private router: Router) {
        if(!this.testsService.allTests){
            let serviceApi = 'assets/jsons/tests.json';
            this.testsService
                .getTests(serviceApi)
                .subscribe(ret => {this.testNames = ret; this.testsService.allTests = ret});
        }
        else{
            this.testNames = this.testsService.allTests;
        }

        this.passed = this.testsService.passed;
        this.failed = this.testsService.failed;
    }

    // run given test
    runTest(path){
        this.testsService.setStopNext();
        this.router.navigate(['/tests/testing', path]);
    }

}
