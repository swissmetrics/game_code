import { Component, OnInit, ViewChild } from '@angular/core';
import { TestsService } from '../../services/tests.service';
import { TestMenuComponent }  from './menu.component';
import { TestBillModel } from '../../models/testBill.model';

@Component({
    templateUrl: '../../templates/tests/bill.html'
})

// Hopper test
export class TestBillComponent implements OnInit{

    @ViewChild(TestMenuComponent)
    private testMenuComponent: TestMenuComponent;

    public title: string;
    public channels: Array<TestBillModel>;
    public status: Array<string> = [];
    private testName: string = 'bill';
    public testStatus: string;

    constructor(private testsService: TestsService) {
        this.testsService.setup(this.testName);
    }

    ngOnInit() {

        this.testMenuComponent.isActive = this.testName;

        let serviceApi = 'assets/jsons/test-bills.json';
        this.testsService
            .getBills(serviceApi)
            .subscribe(ret => {
                this.channels = ret;
                this.testit();
            });
    }

    // execute test
    testit(){
        this.testsService.progress();
        this.testMenuComponent.isProgress = this.testName;
        //fake tests
        for(let ch in this.channels){
            ((d: number) => {
                setTimeout(() => {
                    this.status[d] = 'inprogress';
                    setTimeout(() => {
                        this.status[d] = 'success';
                        if(d == this.channels.length - 1){
                            setTimeout(() => {
                                this.success();
                            }, 2000);
                        }
                    },  2500);
                }, d * 3000);

            })(parseInt(ch));
        }
    }

    // do it when test succeed
    success(){
        this.testMenuComponent.nextTestName = this.testsService.nextTest;
        this.testMenuComponent.isSuccess = this.testName;
        this.testsService.finish();
        this.testMenuComponent.passed = this.testsService.passed;
        this.testStatus = this.testsService.testStatus;
    }

    // when the test fails
    fail(){
        this.testsService.fail();
        this.testMenuComponent.failed = this.testsService.failed;
        this.testStatus = this.testsService.testStatus;
    }
}
