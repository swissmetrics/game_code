import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { ParameterModel } from '../../models/parameter.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: '../../templates/setup/parameter.html'
})

export class SetupParameterComponent implements OnInit{

    public parameter: ParameterModel;
    public activeButtons: Map<string, string>;

    constructor(private setupService: SetupService, private translate: TranslateService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-parameter.json';
        this.setupService
            .getParameter(serviceApi)
            .subscribe(ret => {
                this.parameter = ret;
            });
    }

    // save parameter setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putParameter(serviceApi, this.parameter)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.parameter);
                },
                error => {
                    this.setupService.error = true;
                }
            );

        // change language right now
        this.translate.use(this.parameter.parameters.language);
    }

}
