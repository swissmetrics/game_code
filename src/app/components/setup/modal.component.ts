import { Component, DoCheck } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { GameService } from '../../services/game.service';
import { InitialService } from '../../services/initial.service';
import { ClearRamModel } from '../../models/initialClearRam.model';

@Component({
    selector: 'modal-setup',
    templateUrl: '../../templates/setup/modal.html'
})

export class SetupModalComponent implements DoCheck{

    public saved: boolean;
    public error: boolean;
    private clearRam: ClearRamModel;

    constructor(
        private setupService: SetupService,
        private gameService: GameService,
        private initialService: InitialService) {
            this.saved = false;
            this.error = false;
    }

    ngDoCheck() {
        this.saved = this.setupService.saved || this.gameService.saved || this.initialService.saved;
        this.error = this.setupService.error || this.gameService.error || this.initialService.error;
    }

    // RAM clear before opening Initial Setup
    clearingRam(): void{
        this.clearRam = {is_configured: true};
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putClearRam(serviceApi, this.clearRam)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                },
                error => {
                    this.initialService.error = true;
                }
            );

        //fake RAM clear once again after 5 min
        this.initialService.isRamClear = false;
        setTimeout(()=>{
            this.initialService.isRamClear = true;
        }, 300000);
    }
}
