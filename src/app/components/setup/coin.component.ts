import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { AcceptorModel } from '../../models/acceptor.model';

@Component({
    templateUrl: '../../templates/setup/coin-acceptor.html'
})

export class SetupCoinComponent implements OnInit{

    public acceptor: AcceptorModel;
    public activeButtons: Map<string, string>;
    public params: {
        general: {options: string[]},
        channels: {options: string[]}
    };

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
        this.params = {
            general: {
                options: ['on', 'off']
            },
            channels: {
                options: ['on', 'off']
            }
        };
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-coin-acceptor.json';
        this.setupService
            .getAcceptor(serviceApi)
            .subscribe(ret => {
                this.acceptor = ret;
            });
    }

    // save coin acceptor setup
    save(): void{

        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putAcceptor(serviceApi, this.acceptor)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.acceptor);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

}
