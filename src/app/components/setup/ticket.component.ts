import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { TicketModel } from '../../models/ticket.model';

@Component({
    templateUrl: '../../templates/setup/ticket.html'
})

export class SetupTicketComponent implements OnInit{

    public ticket: TicketModel;
    public activeButtons: Map<string, string>;
    public params: {
        date: {options: string[]},
        handapy: {options: string[]},
        ticket: {options: string[]}
    };

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
        this.params = {
            date: {
                options: ['YYYY-MM-DD', 'MM/DD/YYYY', 'DD/MM/YYYY']
            },
            handapy: {
                options: ['on', 'off']
            },
            ticket: {
                options: ['on', 'off']
            }
        };
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-ticket.json';
        this.setupService
            .getTicket(serviceApi)
            .subscribe(ret => {
                this.ticket = ret;
            });
    }

    // save limit setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putTicket(serviceApi, this.ticket)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.ticket);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

}
