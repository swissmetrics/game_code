import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { LimitModel } from '../../models/limit.model';

@Component({
    templateUrl: '../../templates/setup/limit.html'
})

export class SetupLimitComponent implements OnInit{

    public limit: LimitModel;

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-limit.json';
        this.setupService
            .getLimit(serviceApi)
            .subscribe(ret => {
                this.limit = ret;
            });
    }

    // save hopper setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putLimit(serviceApi, this.limit)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.limit);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

}
