import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { HopperModel } from '../../models/hopper.model';

@Component({
    templateUrl: '../../templates/setup/hopper.html'
})

export class SetupHopperComponent implements OnInit{

    public hopper: HopperModel;
    public activeButtons: Map<string, string>;
    public params: {
        general: {options: string[]}
    };
    public cI: number;

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
        this.params = {
            general: {
                options: ['on', 'off']
            }
        };
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-hopper.json';
        this.setupService
            .getHopper(serviceApi)
            .subscribe(ret => {
                this.hopper = ret;
            });
    }

    // save hopper setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putHopper(serviceApi, this.hopper)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.hopper);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

}
