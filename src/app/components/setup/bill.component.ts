import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { AcceptorModel } from '../../models/acceptor.model';

@Component({
    templateUrl: '../../templates/setup/bill-acceptor.html'
})

export class SetupBillComponent implements OnInit{

    public acceptor: AcceptorModel;
    public activeButtons: Map<string, string>;
    public params: {
        general: {options: string[]},
        channels: {options: string[]},
        directions: {options: string[]}
    };

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
        this.params = {
            general: {
                options: ['on', 'off']
            },
            channels: {
                options: ['on', 'off']
            },
            directions: {
                options: ['A', 'B', 'C', 'D']
            }
        };
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-bill-acceptor.json';
        this.setupService
            .getAcceptor(serviceApi)
            .subscribe(ret => {
                this.acceptor = ret;
            });
    }

    // save coin acceptor setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putAcceptor(serviceApi, this.acceptor)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.acceptor);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

    // set bill direction
    setDirection(direction: string, position: number){

        if(this.acceptor.directions.indexOf(direction) !== -1){
            this.acceptor.directions[position] = '';
        }
        else{
            this.acceptor.directions[position] = direction;
        }
    }
}
