import { Component, OnInit } from '@angular/core';
import { SetupService } from '../../services/setup.service';
import { PrinterModel } from '../../models/printer.model';

@Component({
    templateUrl: '../../templates/setup/printer.html'
})

export class SetupPrinterComponent implements OnInit{

    public printer: PrinterModel;
    public activeButtons: Map<string, string>;
    public params: {
        general: {options: string[]}
    };

    constructor(private setupService: SetupService) {
        this.setupService.saved = false;
        this.activeButtons = new Map<string, string>();
        this.params = {
            general: {
                options: ['on', 'off']
            }
        };
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/setup-printer.json';
        this.setupService
            .getPrinter(serviceApi)
            .subscribe(ret => {
                this.printer = ret;
            });
    }

    // save hopper setup
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.setupService
            .putPrinter(serviceApi, this.printer)
            .subscribe(
                ret => {
                    this.setupService.saved = true;
                    console.log(this.printer);
                },
                error => {
                    this.setupService.error = true;
                }
            );
    }

}
