import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { PaginationComponent } from '../pagination.component';
import { AccountingService } from '../../services/accounting.service';
import { SecurityModel } from '../../models/accountingSecurity.model';
import { SecurityHistoryModel } from '../../models/accountingSecurityHistory.model';

@Component({
    templateUrl: '../../templates/accounting/security.html',
    providers: [ AccountingService ]
})

export class AccountingSecurityComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public security: SecurityModel;
    public history: SecurityHistoryModel;
    private pageLimit: number = 10;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-security.json';
        this.sccountingService
            .getSecurity(serviceApi)
            .subscribe(ret => {
                this.security = ret;
            });
        this.onPageChange(1);
    }

    // change page in paginator
    onPageChange(page){
        let offset = this.pageLimit * (page - 1);
        let serviceApi = 'assets/jsons/accounting-security-history.json';
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.sccountingService
            .getSecurityHistory(serviceApi)
            .subscribe(ret => {
                this.history = ret;
                this.paginationComponent.getPager(ret.count, page);
            });
    }

}
