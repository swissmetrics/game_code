import { Component, ViewChild } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { AccountingService } from '../../services/accounting.service';
import { ProgressiveModel } from '../../models/accountingProgressive.model';

@Component({
    templateUrl: '../../templates/accounting/progressive.html',
    providers: [ AccountingService ]
})

export class AccountingProgressiveComponent{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    public progressive: ProgressiveModel;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-progressive.json';
        this.sccountingService
            .getProgressive(serviceApi)
            .subscribe(ret => {
                this.progressive = ret;
            });
    }

}
