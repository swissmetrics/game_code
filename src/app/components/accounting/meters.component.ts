import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { AccountingService } from '../../services/accounting.service';
import { AccountingMasterModel } from '../../models/accountingMaster.model';

@Component({
    templateUrl: '../../templates/accounting/meters.html',
    providers: [ AccountingService ]
})

export class AccountingMetersComponent implements OnInit {

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    public meters: AccountingMasterModel;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-master.json';
        this.sccountingService
            .getMaster(serviceApi)
            .subscribe(ret => {
                this.meters = ret;
            });
    }
}
