import { Component, ViewChild } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';

@Component({
    templateUrl: '../../templates/accounting/error.html'
})

export class AccountingErrorComponent{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    constructor(){
    }

}
