import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { PaginationComponent } from '../pagination.component';
import { AccountingService } from '../../services/accounting.service';
import { PowerModel } from '../../models/accountingPower.model';
import { PowerHistoryModel } from '../../models/accountingPowerHistory.model';

@Component({
    templateUrl: '../../templates/accounting/power.html',
    providers: [ AccountingService ]
})

export class AccountingPowerComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public power: PowerModel;
    public history: PowerHistoryModel;
    private pageLimit: number = 10;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-power.json';
        this.sccountingService
            .getPower(serviceApi)
            .subscribe(ret => {
                this.power = ret;
            });
        this.onPageChange(1);
    }

    // change page in paginator
    onPageChange(page){
        let offset = this.pageLimit * (page - 1);
        let serviceApi = 'assets/jsons/accounting-power-history.json';
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.sccountingService
            .getPowerHistory(serviceApi)
            .subscribe(ret => {
                this.history = ret;
                this.paginationComponent.getPager(ret.count, page);
            });
    }

}
