import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { PaginationComponent } from '../pagination.component';
import { AccountingService } from '../../services/accounting.service';
import { BillModel } from '../../models/accountingBill.model';
import { BillHistoryModel } from '../../models/accountingBillHistory.model';

@Component({
    templateUrl: '../../templates/accounting/acceptor.html',
    providers: [ AccountingService ]
})

export class AccountingAcceptorComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public bill: BillModel;
    public history: BillHistoryModel;
    private pageLimit: number = 10;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-bill-acceptor.json';
        this.sccountingService
            .getBillAcceptor(serviceApi)
            .subscribe(ret => {
                this.bill = ret;
            });
        this.onPageChange(1);
    }

    // change page in paginator
    onPageChange(page){
        let offset = this.pageLimit * (page - 1);
        let serviceApi = 'assets/jsons/accounting-bill-acceptor-history.json';
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.sccountingService
            .getBillHistory(serviceApi)
            .subscribe(ret => {
                this.history = ret;
                this.paginationComponent.getPager(ret.count, page);
            });
    }
}
