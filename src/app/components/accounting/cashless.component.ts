import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { PaginationComponent } from '../pagination.component';
import { AccountingService } from '../../services/accounting.service';
import { CashlessModel } from '../../models/accountingCashless.model';
import { CashlessHistoryModel } from '../../models/accountingCashlessHistory.model';

@Component({
    templateUrl: '../../templates/accounting/cashless.html',
    providers: [ AccountingService ]
})

export class AccountingCashlessComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public cashless: CashlessModel;
    public history: CashlessHistoryModel;
    private pageLimit: number = 10;
    public currency: string;

    constructor(private accountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-cashless.json';
        this.accountingService
            .getCashless(serviceApi)
            .subscribe(ret => {
                this.cashless = ret;
                this.currency = ret.currency;
            });
        this.onPageChange(1);
    }

    // change page in paginator
    onPageChange(page){
        let offset = this.pageLimit * (page - 1);
        let serviceApi = 'assets/jsons/accounting-cashless-history.json';
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.accountingService
            .getCashlessHistory(serviceApi)
            .subscribe(ret => {
                this.history = ret;
                this.paginationComponent.getPager(ret.count, page);
            });
    }

    // return existing key from i18
    whatExist(keys: string[]): string {
        for(let v of keys) {
            if(v.indexOf('ACCOUNTING.CASHLESS') === -1) {
                return v;
            }
        }
        return '';
    }

    // if print currency prefix
    isCurrency(key: string): string {
        let noCurrency = ['PRINTER.accepted', 'PRINTER.printed',' CARD.accepted'];
        if(noCurrency.indexOf(key) === -1) {
            return this.currency;
        }
        return '';
    }
}
