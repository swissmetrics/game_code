import { Component, ViewChild, OnInit } from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { PaginationComponent } from '../pagination.component';
import { AccountingService } from '../../services/accounting.service';
import { HopperModel } from '../../models/accountingHopper.model';
import { HopperHistoryModel } from '../../models/accountingHopperHistory.model';

@Component({
    templateUrl: '../../templates/accounting/hopper.html',
    providers: [ AccountingService ]
})

export class AccountingHopperComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public hopper: HopperModel;
    public history: HopperHistoryModel;
    private pageLimit: number = 10;

    constructor(private sccountingService: AccountingService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/accounting-hopper.json';
        this.sccountingService
            .getHopper(serviceApi)
            .subscribe(ret => {
                this.hopper = ret;
            });
        this.onPageChange(1);
    }

    // change page in paginator
    onPageChange(page){
        let offset = this.pageLimit * (page - 1);
        let serviceApi = 'assets/jsons/accounting-hopper-history.json';
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.sccountingService
            .getHopperHistory(serviceApi)
            .subscribe(ret => {
                this.history = ret;
                this.paginationComponent.getPager(ret.count, page);
            });
    }
}
