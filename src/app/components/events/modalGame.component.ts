import { Component, Output, EventEmitter } from '@angular/core';
import { GameService } from '../../services/game.service';
import { GameParamsModel } from '../../models/gameParams.model';

@Component({
    selector: 'modal-game',
    templateUrl: '../../templates/events/modal-game.html'
})

export class ModalGameComponent {

    @Output() prevNextButton = new EventEmitter();

    public params: GameParamsModel;
    private gameId: number;
    private nextId: number;
    private prevId: number;

    constructor(private gameService: GameService){
    }

    // load game detail
    loadContent(gameId: number, recordId: number){
        let serviceApi = `assets/jsons/game-history-params-${gameId}-${recordId}.json`;
        this.gameId = gameId;
        this.gameService
            .getGameParams(serviceApi)
            .subscribe(ret => {
                this.params = ret;
                this.nextId = ret.next_id;
                this.prevId = ret.prev_id;
            });
    }

    // load previous game
    loadPrev(){
        this.loadContent(this.gameId, this.prevId);
        this.emitClick(this.prevId);
    }

    // load next game
    loadNext(){
        this.loadContent(this.gameId, this.nextId);
        this.emitClick(this.nextId);
    }

    // emit event to parent component
    emitClick(e) {
        this.prevNextButton.emit(e);
    }
}
