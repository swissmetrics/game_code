import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { EventsService } from '../../services/events.service';
import { TimeFiltersComponent } from '../timeFilters.component';
import { ModalEventComponent } from './modalEvent.component';
import { PaginationComponent } from '../pagination.component';
import { EventsModel } from '../../models/events.model';

@Component({
    templateUrl: '../../templates/events/logs.html'
})

// all events
export class LogsComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(ModalEventComponent)
    private modalEventComponent: ModalEventComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public types: Array<string>;
    public names: Array<string>;
    public events: EventsModel;
    public pageLimit: number = 10;
    public eventKind: string = '';
    public eventName: string = ''
    public startDate: string;
    public endDate: string;
    public selectedRow: number;

    constructor(private eventsService: EventsService) {
    }

    ngOnInit() {
        this.getTypes();
        this.getNames();
        this.getEvents();
    }

    // event types
    getTypes(){

        let serviceApi = 'actions/types';

        this.eventsService
            .getEventTypes(serviceApi)
            .subscribe(
                ret => { this.types = ret },
                error => { console.log('Error: api/actions/types') }
            );
    }

    // event names
    getNames(){

        let serviceApi = 'actions/names';

        this.eventsService
            .getEventNames(serviceApi)
            .subscribe(
                ret => { this.names = ret },
                error => { console.log('Error: api/actions/names') }
            );
    }

    // load events, kind - type of event
    getEvents(page:number = 1){

        let serviceApi = 'actions';

        let offset = this.pageLimit * (page - 1);
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        serviceApi += this.eventKind ? `&type=${this.eventKind}` : '';
        serviceApi += this.eventName ? `&name=${this.eventName}` : '';
        serviceApi += this.startDate ? `&start_date=${this.startDate}` : '';
        serviceApi += this.endDate ? `&end_date=${this.endDate}` : '';

        this.eventsService
            .getEvents(serviceApi)
            .subscribe(
                ret => {
                    this.events = ret;
                    this.paginationComponent.getPager(ret.count, page);
                },
                error => { console.log('Error: api/actions') }
            );

        this.selectedRow = -1;
    }

    // payload can have various structure
    showPayload(payload){
        if(typeof payload === 'object'){
            return payload.repr ? payload.repr : 'N/A';
        }
        return payload;
    }

    // press the menu button
    onTabTypeClick(type:string){
      this.eventKind = type;
      this.getEvents();
    }

    // press the menu button
    onTabNameClick(name:string){
      this.eventName = name;
      this.getEvents();
    }

    // change page in paginator
    onPageChange(page){
        this.getEvents(page);
    }

    // change date range
    onDateChange(e){
        console.log(e)
        this.startDate = e.from;
        this.endDate = e.to;
        this.getEvents();
    }

    // load content to modal window
    callModal(eventId: number){
        this.modalEventComponent.loadContent(333);
    }

    // select row in the table
    selectRow(index: number){
        this.selectedRow = index;
    }
}
