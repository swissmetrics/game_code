import { Component, ViewChild } from '@angular/core';
import { EventsService } from '../../services/events.service';
import { EventDetailModel } from '../../models/eventDetail.model';
import { ModalGameComponent } from './modalGame.component';

@Component({
    selector: 'modal-event',
    templateUrl: '../../templates/events/modal-event.html'
})

export class ModalEventComponent {

    @ViewChild(ModalGameComponent)
    private modalGameComponent: ModalGameComponent;

    public details: EventDetailModel;

    constructor(private eventsService: EventsService){
    }

    // load event details
    loadContent(eventId: number){
        let serviceApi = `assets/jsons/events-detail-${eventId}.json`;
        this.eventsService
            .getDetail(serviceApi)
            .subscribe(ret => {
                this.details = ret;
            });
    }

    // load more detail to modal
    moreDetails(eventId: number, eventType: string){
        if(eventType == 'game'){
            this.modalGameComponent.loadContent(1, 1);
        }
    }
}
