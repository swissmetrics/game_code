import { Component, OnInit, ViewChild} from '@angular/core';
import { TimeFiltersComponent } from '../timeFilters.component';
import { ModalGameComponent } from './modalGame.component';
import { PaginationComponent } from '../pagination.component';
import { GameService } from '../../services/game.service';
import { GameHistoryModel } from '../../models/gameHistory.model';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
    templateUrl: '../../templates/events/game-history.html'
})

export class GameHistoryComponent implements OnInit{

    @ViewChild(TimeFiltersComponent)
    private timeFiltersComponent: TimeFiltersComponent;

    @ViewChild(ModalGameComponent)
    private modalGameComponent: ModalGameComponent;

    @ViewChild(PaginationComponent)
    private paginationComponent: PaginationComponent;

    public events: GameHistoryModel;
    public gameName: string = '';
    public pageTotal: number;
    public pageLimit: number = 10;
    private gameId: number;
    public selectedRow: number;

    constructor(
        private route: ActivatedRoute,
        private gameService: GameService) {
    }

    ngOnInit() {
        this.gameId = +this.route.snapshot.paramMap.get('id');
        this.getHistory();
    }

    // load game history
    getHistory(page:number = 1){

        let serviceApi = `assets/jsons/game-history-${this.gameId}.json`;

        let offset = this.pageLimit * (page - 1);
        serviceApi += `?limit=${this.pageLimit}&offset=${offset}`;
        this.gameService
            .getGameHistory(serviceApi)
            .subscribe(
                ret => {
                    this.events = ret;
                    this.gameName = ret.game.name;
                    this.pageTotal = ret.count;
                    this.paginationComponent.getPager(ret.count, page);
                },
                error => { console.log('Error: api/actions') }
            );
        this.selectedRow = -1;
    }

    // change page in paginator
    onPageChange(page){
        this.getHistory(page);
    }

    // click prev/next in modal window
    onModalChange(eventId: number){
        this.selectRow(eventId);
    }

    // load content to modal window
    callModal(gameId: number, recordId: number){
        this.modalGameComponent.loadContent(gameId, recordId);
    }

    // select row in the table
    selectRow(index: number){
        this.selectedRow = index;
    }
}
