import { Component } from '@angular/core';

export interface gameParams{
    id: number,
    name: string,
    theoretical_rtp: number;
    real_rtp: number;
    bet_per_game: number;
    total_in: number;
    total_out: number;
}

@Component({
    selector: 'modal-small-game',
    templateUrl: '../../templates/events/modal-small-game.html'
})

export class ModalSmallGameComponent {

    public params: gameParams;

    constructor(){
    }

    // load game detail
    loadContent(params: gameParams) {
        this.params = params;
    }

}
