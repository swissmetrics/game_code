import { Component, OnInit } from '@angular/core';
import { AttendantService } from '../../services/attendant.service';
import { HandpayModel } from '../../models/attendantHandpay.model';

@Component({
    templateUrl: '../../templates/attendant/handpay.html',
    providers: [ AttendantService ]
})

export class AttendantHandpayComponent implements OnInit{

    public handpay: HandpayModel;
    public objectKeys: typeof Object.keys = Object.keys;

    constructor(private attendantService: AttendantService){
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/attendant-handpay.json';
        this.attendantService
            .getHandpay(serviceApi)
            .subscribe(ret => {
                this.handpay = ret;
            });
    }
}
