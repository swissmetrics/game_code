import { Component, OnInit } from '@angular/core';
import { Router }  from '@angular/router';
import { AttendantService } from '../../services/attendant.service';
import { DumpModel } from '../../models/attendantDump.model';

@Component({
    templateUrl: '../../templates/attendant/dump.html',
    providers: [ AttendantService ]
})

export class AttendantDumpComponent implements OnInit {

    private activeValue: number;
    private payOut: Object = {};
    private ratio: number;
    private totalDump: number = 0;
    private lastOperation: number = 0;
    private canClear: boolean = false;
    private totalaLevel: number;
    private periodicLevel: number;
    private ready: boolean = false;
    private initialLevels: Array<number>;
    public dump: DumpModel;

    constructor(
        private router: Router,
        private attendantService: AttendantService) {
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/attendant-dump.json';
        this.attendantService
            .getHopperDump(serviceApi)
            .subscribe(ret => {
                this.dump = ret;
                for(let level of ret.settings.levels) {
                    this.initialLevels = [level.total, level.periodic];
                    this.totalaLevel = level.total;
                    this.periodicLevel = level.periodic;
                    this.ratio = level.ratio;
                }
            });
    }

    // determine amount dumped coins
    addCoins(coins: number){
        this.ready = true;
        this.canClear = false;
        this.totalDump += coins;
        this.totalaLevel -= coins;
        this.periodicLevel -= coins;
        this.lastOperation = coins;
        if(this.totalDump > this.initialLevels[0]){
            this.totalDump = this.initialLevels[0];
            this.totalaLevel = 0;
        }
        if(this.periodicLevel < 0){
            this.periodicLevel = 0;
        }
    }

    // cancel the last operation
    cancelCoins(){
        this.totalDump -= this.lastOperation;
        this.totalaLevel += this.lastOperation;
        this.periodicLevel += this.lastOperation;
        this.lastOperation = 0;
        this.canClear = true;
    }

    // clear all operations
    clearCoins(){
        this.totalDump = 0;
        this.totalaLevel = this.initialLevels[0];
        this.periodicLevel = this.initialLevels[1];
        this.lastOperation = 0;
        this.canClear = false;
        this.ready = false;
    }

    // exit hopper dump
    close(){
        this.router.navigate(['/attendant']);
    }
}
