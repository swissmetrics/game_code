import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'modal-datapicker',
    templateUrl: '../templates/datapicker.html'
})

export class DatapickerComponent {

    @Output() emitCustomDate = new EventEmitter();

    public fromDate: Date;
    public toDate: Date;
    public fromHour: string;
    public toHour: string;
    public fromHourShow: boolean;
    public toHourShow: boolean;
    public timePossition: number = 0;
    public allowedKeys: {};
    public keys: Array<number>;
    private prevValue: number = 0;
    private originFromHour: string;
    private originToHour: string;

    constructor() {
        this.fromDate = new Date();
        this.toDate = new Date();
        this.fromHour = '00:00:00';
        this.toHour = '23:59:00';
        this.keys = new Array(10);
        this.allowedKeys = {
            0: [0,1,2],
            1: [0,1,2,3,4,5,6,7,8,9],
            2: [0,1,2,3,4,5],
            4: [0,1,2,3,4,5,6,7,8,9],
            5: [0,1,2,3,4,5],
            7: [0,1,2,3,4,5,6,7,8,9],
            8: [0,1,2,3,4,5,6,7,8,9]
        };
    }

    setFromHour(hour: number, minute: number){
        this.fromDate.setHours(hour);
        this.fromDate.setMinutes(minute);
        return this.fromDate;
    }

    setToHour(hour: number, minute: number){
        this.toDate.setHours(hour);
        this.toDate.setMinutes(minute);
        return this.toDate;
    }

    customDate(){
        let parseFrom = this.fromHour.split(':');
        let parseTo = this.toHour.split(':');
        this.fromDate.setHours(parseInt(parseFrom[0]), parseInt(parseFrom[1]), parseInt(parseFrom[2]));
        this.toDate.setHours(parseInt(parseTo[0]), parseInt(parseTo[1]), parseInt(parseTo[2]));
        this.emitCustomDate.emit({from:this.fromDate, to: this.toDate});
    }

    // init form Time filter buttons
    initCalendar(from?: number, to?: number){
        this.fromDate = from ? new Date(from * 1000) : new Date();
        this.toDate = to ? new Date(to * 1000): new Date();
        let fdT = [this.fromDate.getHours(), this.fromDate.getMinutes(), this.fromDate.getSeconds()];
        let tdT = [this.toDate.getHours(), this.toDate.getMinutes(), this.toDate.getSeconds()];
        this.fromHour = `${(fdT[0] < 10 ? '0' : '')+fdT[0]}:${(fdT[1] < 10 ? '0' : '')+fdT[1]}:${(fdT[2] < 10 ? '0' : '')+fdT[2]}`;
        this.toHour = `${(tdT[0] < 10 ? '0' : '')+tdT[0]}:${(tdT[1] < 10 ? '0' : '')+tdT[1]}:${(tdT[2] < 10 ? '0' : '')+tdT[2]}`;
    }

    closePopUps(){
        this.fromHourShow = false;
        this.toHourShow = false;
        if(this.fromHour.indexOf('_') !== -1){
            this.fromHour = this.originFromHour;
        }
        if(this.toHour.indexOf('_') !== -1){
            this.toHour = this.originToHour;
        }
    }

    clickFromTime(){
        this.originFromHour = this.fromHour;
        this.fromHourShow = true;
        this.fromHour = '__:__:__';
        this.timePossition = 0;
    }

    clickToTime(){
        this.originToHour = this.toHour;
        this.toHourShow = true;
        this.toHour = '__:__:__';
        this.timePossition = 0;
    }

    insertTimeFrom(value: string){
        this.fromHour =  this.formatTime(value, this.fromHour);
    }

    insertTimeTo(value: string){
        this.toHour =  this.formatTime(value, this.toHour);
    }

    // display time in the format hh:mm:ss
    formatTime(value: string, ctx: string){
        let arrHour: string[] = ctx.split('');

        if(this.timePossition == 2 || this.timePossition == 5){
            ++this.timePossition;
        }

        arrHour[this.timePossition] = value;
        ctx = arrHour.join('');

        this.timePossition++;

        if(this.timePossition > 7) {
            this.fromHourShow = false;
            this.toHourShow = false;
        }

        this.prevValue = parseInt(value);
        this.allowedKeys[1] = (this.prevValue == 2 ? [0,1,2,3] : [0,1,2,3,4,5,6,7,8,9]);

        return ctx;
    }
}
