import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { DatapickerComponent } from './datapicker.component';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'time-filters',
    templateUrl: '../templates/time-filters.html',
    providers: [DatePipe]
})

export class TimeFiltersComponent{

    @ViewChild(DatapickerComponent)
    private datapickerComponent: DatapickerComponent;

    @Output() buttonClicked = new EventEmitter();

    public timeFilter: string = 'all';
    public customRange: string;

    constructor(private datePipe: DatePipe){
    }

    calculateSeconds(time: number){
        return Math.floor(time / 1000);
    }

    calculate_24h(nowSec, daySec){
        let fromDate = (nowSec - daySec);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    calculate_yesterday(nowSec, daySec){
        let fromDate, toDate;
        fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - 1);
        fromDate.setHours(0, 0, 0);
        fromDate = this.calculateSeconds(fromDate);
        toDate = fromDate + daySec - 1;
        this.datapickerComponent.initCalendar(fromDate, toDate);
        return {from: fromDate, to: toDate};
    }

    calculate_today(nowSec, daySec){
        let fromDate;
        fromDate = new Date();
        fromDate.setHours(0, 0, 0);
        fromDate = this.calculateSeconds(fromDate);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    calculate_this_week(nowSec, daySec){
        let fromDate;
        fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - (fromDate.getDay() - 1));
        fromDate.setHours(0, 0, 0);
        fromDate = this.calculateSeconds(fromDate);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    calculate_last_week(nowSec, daySec){
        let fromDate = (nowSec - 7 * daySec);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    calculate_this_month(nowSec, daySec){
        let fromDate;
        fromDate = new Date();
        fromDate.setDate(1);
        fromDate.setHours(0, 0, 0);
        fromDate = this.calculateSeconds(fromDate);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    calculate_last_month(nowSec, daySec){
        let fromDate = (nowSec - 30 * daySec);
        this.datapickerComponent.initCalendar(fromDate);
        return {from: fromDate, to: nowSec};
    }

    changeTime(filter: string){

        this.timeFilter = filter;
        let nowSec = this.calculateSeconds( new Date().getTime() );
        let daySec  = 24 * 3600;
        let range = this['calculate_'+filter](nowSec, daySec);

        this.emitEvent(range.from, range.to)
        this.displayRange(range.from, range.to)
    }

    setDefault(){
        this.timeFilter = 'all';
        this.customRange = '';
        this.emitEvent(null, null)
    }

    customTime(e:{from, to}){
        let fromDateSec = Math.floor(e.from / 1000);
        let toDateSec = Math.floor(e.to / 1000);
        this.emitEvent(fromDateSec, toDateSec);
        this.displayRange(fromDateSec, toDateSec)
    }

    // display date range in the button
    displayRange(from, to){
        let formatedFrom = this.datePipe.transform(from * 1000, 'dd/MM/yyyy HH:mm:ss');
        let formatedTo = this.datePipe.transform(to * 1000, 'dd/MM/yyyy HH:mm:ss');
        this.customRange = `${formatedFrom} - ${formatedTo}`;
    }

    // emit event to parent component
    emitEvent(from: number, to: number|boolean = null) {
        this.buttonClicked.emit({from:from, to: to});
    }
}
