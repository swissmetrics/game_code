import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { NetworkModel } from '../../models/initialNetwork.model';

@Component({
    templateUrl: '../../templates/initial/network.html'
})

export class InitialNetworkComponent implements OnInit{

    public network: NetworkModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-network.json';
        this.initialService
            .getNetwork(serviceApi)
            .subscribe(ret => {
                this.network = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putNetwork(serviceApi, this.network)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.network);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }
}
