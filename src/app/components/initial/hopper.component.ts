import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { HopperModel } from '../../models/initialHopper.model';

@Component({
    templateUrl: '../../templates/initial/hopper.html'
})

export class InitialHopperComponent implements OnInit{

    public hopper: HopperModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-hopper.json';
        this.initialService
            .getHopper(serviceApi)
            .subscribe(ret => {
                this.hopper = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putHopper(serviceApi, this.hopper)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.hopper);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }

    // enable/disable choosen channel
    enableChannel(index: number, status: 'on'|'off' = 'off'){
        this.hopper.settings.channels[index].enabled = status;
    }
}
