import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { CoinModel } from '../../models/initialCoin.model';

@Component({
    templateUrl: '../../templates/initial/coin-acceptor.html'
})

export class InitialCoinAcceptorComponent implements OnInit{

    public coin: CoinModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-coin.json';
        this.initialService
            .getCoin(serviceApi)
            .subscribe(ret => {
                this.coin = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putCoin(serviceApi, this.coin)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.coin);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }

    // enable/disable choosen channel
    enableChannel(index: number, status: 'on'|'off' = 'off'){
        this.coin.settings.channels[index].enabled = status;
    }
}
