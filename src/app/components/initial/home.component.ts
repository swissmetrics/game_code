import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';

@Component({
    templateUrl: '../../templates/initial/home.html'
})

export class InitialHomeComponent implements OnInit {

    constructor(private initialService: InitialService) {
    }

    ngOnInit() {
        this.initialService.clearRam();
    }
}
