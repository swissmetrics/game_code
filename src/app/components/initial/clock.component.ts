import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { ClockModel } from '../../models/initialClock.model';

@Component({
    templateUrl: '../../templates/initial/clock.html'
})

export class InitialClockComponent implements OnInit{

    public clock: ClockModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-clock.json';
        this.initialService
            .getClock(serviceApi)
            .subscribe(ret => {
                this.clock = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putClock(serviceApi, this.clock)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.clock);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }
}
