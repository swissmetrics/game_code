import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { BillModel } from '../../models/initialBill.model';

@Component({
    templateUrl: '../../templates/initial/bill-acceptor.html'
})

export class InitialBillAcceptorComponent implements OnInit{

    public bill: BillModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-bill.json';
        this.initialService
            .getBill(serviceApi)
            .subscribe(ret => {
                this.bill = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putBill(serviceApi, this.bill)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.bill);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }

    // enable/disable choosen channel
    enableChannel(index: number, status: 'on'|'off' = 'off'){
        this.bill.settings.channels[index].enabled = status;
    }

}
