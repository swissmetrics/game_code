import { Component, OnInit } from '@angular/core';
import { InitialService } from '../../services/initial.service';
import { GeneralModel } from '../../models/initialGeneral.model';

@Component({
    templateUrl: '../../templates/initial/general.html'
})

export class InitialGeneralComponent implements OnInit{

    public generals: GeneralModel;
    public activeButtons: Map<string, string>;

    constructor(private initialService: InitialService){
        this.activeButtons = new Map<string, string>();
    }

    ngOnInit() {
        let serviceApi = 'assets/jsons/initial-general.json';
        this.initialService
            .getGenerals(serviceApi)
            .subscribe(ret => {
                this.generals = ret;
            });
    }

    // save general parameters
    save(): void{
        let serviceApi = 'assets/jsons/save.json';
        this.initialService
            .putGenerals(serviceApi, this.generals)
            .subscribe(
                ret => {
                    this.initialService.saved = true;
                    console.log(this.generals);
                },
                error => {
                    this.initialService.error = true;
                }
            );
    }
}
