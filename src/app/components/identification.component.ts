import { Component, OnInit, ViewChild } from '@angular/core';
import { WebsocketService } from '../services/websocket.service';
import { ModalSmallGameComponent } from './events/modalSmallGame.component';
import { IdentificationService } from '../services/identification.service';
import { IdentificationModel } from '../models/identification.model';

@Component({
    selector: 'app-comment',
    templateUrl: '../templates/identification.html'
})

// machine identification
export class IdentificationComponent implements OnInit{

    @ViewChild(ModalSmallGameComponent)
    private modalSmallGameComponent: ModalSmallGameComponent;

    public identification: IdentificationModel;

    constructor(
        private websocket: WebsocketService,
        private identificationService: IdentificationService) {
    }

    ngOnInit() {
        // get configuration by WS
        let socket = this.websocket.connect();
        let channelServer = this.websocket.channel(socket);
        // temporarily by REST
        let serviceApi = 'assets/jsons/identification-ws.json';
        this.identificationService
            .getIdentification(serviceApi)
            .subscribe(ret => {
                this.identification = ret;
            });
    }

    // load game modal window
    callGameModal(gameId: number) {
        for (let game of this.identification.games) {
            if (game.id == gameId) {
                this.modalSmallGameComponent.loadContent(game);
                break;
            }
        }
    }

    // return translated value from i18 if exists
    whatExist(translated: string, origin: string|number) {
        if (translated.indexOf('IDENTIFICATION.PANELS.VALUES') === -1) {
            return translated;
        }
        return origin;
    }
}
