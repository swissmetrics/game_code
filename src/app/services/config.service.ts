import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { ConfigModel } from '../models/config.model';

@Injectable()
export class ConfigService {

    public config: ConfigModel;

    constructor(private restService: RestService) {
    }

    // GET Configuration -> Replace WS in the future
    getConfig(service: string): Observable<ConfigModel> {
        return this.restService.getAssets(service);
    }

}
