import {Injectable} from '@angular/core';
import {Socket} from 'phoenix/assets/js/phoenix'
import {environment} from '../../environments/environment';

@Injectable()
export class WebsocketService {

    // connect to the server
    connect(tokenValue:string = ''){

        let socket = new Socket(environment.wsServer, {params: {token: tokenValue}});
        socket.connect();

        return socket;
    }

    // choose channel
    channel(socket:any, channelName:string = 'actions'){

        let channel = socket.channel(channelName, {});

        return channel;
    }

    // choose channel & join
    join(socket:any, channelName:string = 'actions'){

        let channel = socket.channel(channelName, {});

        channel.join()
            .receive("ok", () => console.log("WS: Connected"))
            .receive("error", () => console.log("WS: Error"))

        return channel;
    }

}
