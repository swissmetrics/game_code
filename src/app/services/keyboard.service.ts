import { Injectable } from '@angular/core';

@Injectable()
export class KeyboardService {

    public pressedKey: string;
    public currentValue: string;
    public orginalValue: string;
    public eventTarget: any;
    public showKeyboard: boolean;

    constructor() {
        this.pressedKey = '';
        this.currentValue = '';
        this.showKeyboard = false;
    }

}
