import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ConfigService } from './config.service';

@Injectable()
export class AuthService implements CanActivate, CanActivateChild {

    private protectedUrl: { attendant: string[], operator: string[] } = {
        attendant: ['/setup', '/games', '/initial'],
        operator: []
    };

    constructor(
        private router: Router,
        private configService: ConfigService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkUser(state.url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;
        return this.checkUser(state.url);
    }

    checkUrl(segment: string): boolean {
        if (this.protectedUrl[this.configService.config.user].indexOf(segment) !== -1) {
            return false;
        }
        return true;
    }

    checkUser(segment: string): boolean {
        for(let url of this.protectedUrl[this.configService.config.user]) {
            if (segment.indexOf(url) !== -1) {
                this.router.navigate(['/']);
                return false;
            }
        }
        return true;
    }

}
