import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { GeneralModel } from '../models/initialGeneral.model';
import { ClockModel } from '../models/initialClock.model';
import { NetworkModel } from '../models/initialNetwork.model';
import { CoinModel } from '../models/initialCoin.model';
import { BillModel } from '../models/initialBill.model';
import { HopperModel } from '../models/initialHopper.model';
import { ClearRamModel } from '../models/initialClearRam.model';

@Injectable()
export class InitialService {

    public saved: boolean;
    public error: boolean;
    public isRamClear: boolean = true;

    constructor(private restService: RestService) {
    }

    // GET General parameters
    getGenerals(service: string): Observable<GeneralModel>{
        return this.restService.getAssets(service);
    }

    // GET Clock parameters
    getClock(service: string): Observable<ClockModel>{
        return this.restService.getAssets(service);
    }

    // GET Network parameters
    getNetwork(service: string): Observable<NetworkModel>{
        return this.restService.getAssets(service);
    }

    // GET Coin parameters
    getCoin(service: string): Observable<CoinModel>{
        return this.restService.getAssets(service);
    }

    // GET Bill parameters
    getBill(service: string): Observable<BillModel>{
        return this.restService.getAssets(service);
    }

    // GET Hopper parameters
    getHopper(service: string): Observable<HopperModel>{
        return this.restService.getAssets(service);
    }

    // PUT Geenral parameters
    putGenerals(service: string, body: GeneralModel): Observable<GeneralModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Clock parameters
    putClock(service: string, body: ClockModel): Observable<ClockModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Network parameters
    putNetwork(service: string, body: NetworkModel): Observable<NetworkModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Coin parameters
    putCoin(service: string, body: CoinModel): Observable<CoinModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Bill parameters
    putBill(service: string, body: BillModel): Observable<BillModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Hopper parameters
    putHopper(service: string, body: HopperModel): Observable<HopperModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT is_configured param during clear RAM
    putClearRam(service: string, body: ClearRamModel): Observable<ClearRamModel>{
        return this.restService.putAssets(service, body);
    }

    // call modal window to clear ram
    clearRam(){
        if(this.isRamClear){
            $('#modal-clear-ram').modal({backdrop: 'static', keyboard: false});
        }
    }
}
