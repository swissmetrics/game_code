import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { IdentificationModel } from '../models/identification.model';

@Injectable()
export class IdentificationService {

    public saved: boolean;
    public error: boolean;
    public isRamClear: boolean = true;

    constructor(private restService: RestService) {
    }

    // GET identofication parameters
    getIdentification(service: string): Observable<IdentificationModel>{
        return this.restService.getAssets(service);
    }

}
