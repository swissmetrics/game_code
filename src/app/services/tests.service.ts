import { Router }  from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { TestsModel } from '../models/tests.model';
import { TestHopperModel } from '../models/testHopper.model';
import { TestBillModel } from '../models/testBill.model';
import { TestTouchModel } from '../models/testTouch.model';

@Injectable()
export class TestsService {

    public testName: string;
    public nextTest: string;
    public nextTestName: string;
    public allTests: Array<TestsModel>;
    public passed: Array<string> = [];
    public failed: Array<string> = [];
    public testStatus: string;

    constructor(private router: Router, private restService: RestService){
    }

    // GET available tests
    getTests(service: string): Observable<TestsModel[]>{
        return this.restService.getAssets(service);
    }

    // GET coins for hopper
    getCoins(service: string): Observable<TestHopperModel[]>{
        return this.restService.getAssets(service);
    }

    // GET bills for bill acceptor
    getBills(service: string): Observable<TestBillModel[]>{
        return this.restService.getAssets(service);
    }

    // GET points for touchscreen
    getTouchPoints(service: string): Observable<TestTouchModel[]>{
        return this.restService.getAssets(service);
    }

    // do it when test succeed
    success(){
        this.passed.push(this.testName);
        let stopNext = this.getStopNext();
        if(!stopNext){
            setTimeout( () => {
                this.router.navigate(['/tests/testing', this.nextTest]);
            }, 500);
        }
        else{
            this.testStatus = 'ok';
        }
    }

    // when the test is in progress
    progress(){
        this.testStatus = 'progress';
        this.getNextTest();
        console.log(this.testName, 'Progress');
    }

    // when the test fails
    fail(){
        let stopNext = this.getStopNext();
        this.failed.push(this.testName);
        if(stopNext){
            this.testStatus = 'fail';
        }
        else{
            this.goSummary();
        }
    }

    // after all tests
    finish(){
        this.cleanArrays(this.testName);
        this.passed.push(this.testName);
        this.goSummary();
    }

    // redirect to summary page
    goSummary(){
        this.router.navigate(['/tests/testing/summary']);
    }

    // set up service
    setup(testName: string){
        this.testName = testName;
        this.cleanArrays(testName);
    }

    // remove duplicates
    cleanArrays(key){
        this.passed = this.passed.filter(item => item !== key);
        this.failed = this.failed.filter(item => item !== key);
    }

    // set if stop passing to the next test
    setStopNext(){
        sessionStorage.setItem('stopNext', '1');
    }

    // get goNext variable
    getStopNext(){
        return sessionStorage.getItem('stopNext');
    }

    // clear goNext variable
    clearStopNext(){
        sessionStorage.removeItem('stopNext');
        this.passed = [];
        this.failed = [];
    }

    // get test name of the next test
    getNextTest(){
        for(let test in this.allTests){
            if(this.allTests[test].name === this.testName){
                let nextEl = this.allTests[parseInt(test)+1];
                this.nextTestName = nextEl ? nextEl.name : 'none';
                this.nextTest = nextEl ? nextEl.router : '';
            }
        }
    }
}
