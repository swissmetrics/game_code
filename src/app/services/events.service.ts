import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { EventDetailModel } from '../models/eventDetail.model';
import { EventsModel } from '../models/events.model';

@Injectable()
export class EventsService {

    constructor(private restService: RestService) {
    }

    // GET events
    getEvents(service: string): Observable<EventsModel>{
        return this.restService.get(service);
    }

    // GET event detail
    getDetail(service: string): Observable<EventDetailModel>{
        return this.restService.getAssets(service);
    }

    // GET event names
    getEventNames(service: string): Observable<string[]>{
        return this.restService.get(service);
    }

    // GET event types
    getEventTypes(service: string): Observable<string[]>{
        return this.restService.get(service);
    }

}
