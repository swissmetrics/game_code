import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';

@Injectable()
export class NavigationService {

    public tabIndex: number = 0;
    public tabAction: string = '';
    public tabType: string = '';
    private menuSelected: number = 0;
    private menuSelectors: string = '.navbar-nav li a[tabindex]';
    private articleSelectors: string = 'article [tabindex]';
    private selectSelected: boolean = false;

    constructor(private websocket: WebsocketService){
    }

    // listen to navigation buttons
    wsConnect(){

        let socket = this.websocket.connect();
        let channel = this.websocket.join(socket);
        let payload = {
            name: 'insert_coin',
            payload: 111,
            timestamp: Date.now(),
        };

        channel.push("action", payload);

        channel.on("action", msg => {
            // console.log(msg)
        });
    }

    // up button
    goMenu(){
        this.tabType = 'menu';
        this.tabIndex = this.menuSelected;
    }

    // down (select) button
    goDown(){

        let elements: any = document.querySelectorAll(this.articleSelectors);

        if(!elements.length){
            return;
        }

        if(this.tabType == 'menu'){
            this.menuSelected = this.tabIndex;
            elements[0].focus();
        }
        else if(this.tabType == 'select'){
            //elements[this.tabIndex].click();
            this.selectSelected = this.selectSelected ? false : true;
        }
        else{
            elements[this.tabIndex].click();
        }

    }

    // right, left buttons
    nextTab(direction){

        let tabIndex: number = this.tabIndex / 1;
        let next: number = tabIndex + direction;

        switch(this.tabType){
            case 'select':
                if(this.selectSelected){
                    this.selectTab(direction, tabIndex);
                    this.selectSelected = true;
                }
                else{
                    this.inputTab(next);
                    this.selectSelected = false;
                }
                break;
            case 'menu':
                this.menuTab(next);
                break;
            default:
                this.inputTab(next);
        }
    }

    // correct next tab index
    corrNext(n, el) {
        return  n == -1 ? 0 : (n == el.length ? el.length - 1 : n);
    }

    // select element
    setFocus(els, next){
        if(els.length){
            next = this.corrNext(next, els);
            els[next].focus();
        }
    }

    // navigation in the top menu
    menuTab(next:number){
        let elements: any = document.querySelectorAll(this.menuSelectors);
        this.setFocus(elements, next);
    }

    // navigation in article
    inputTab(next:number){
        let elements: any = document.querySelectorAll(this.articleSelectors);
        this.setFocus(elements, next);
    }

    // navigation in select elements
    selectTab(direction, tabIndex){

        let select:any = document.querySelectorAll(this.articleSelectors)[tabIndex];
        let option = select.selectedIndex;

        if(direction === 1){
            select.selectedIndex = option < select.options.length - 1 ? option + 1 : option;
        }
        else{
            select.selectedIndex = option > 0 ? option - 1 : option;
        }
    }

}
