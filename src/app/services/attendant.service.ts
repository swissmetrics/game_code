import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { HandpayModel } from '../models/attendantHandpay.model';
import { RefillModel } from '../models/attendantRefill.model';
import { DumpModel } from '../models/attendantDump.model';

@Injectable()
export class AttendantService {

    constructor(private restService: RestService) {
    }

    // GET handpay
    getHandpay(service: string): Observable<HandpayModel>{
        return this.restService.getAssets(service);
    }

    // GET hopper refill
    getHopperRefill(service: string): Observable<RefillModel>{
        return this.restService.getAssets(service);
    }

    // GET hopper dump
    getHopperDump(service: string): Observable<DumpModel>{
        return this.restService.getAssets(service);
    }

}
