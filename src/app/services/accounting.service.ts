import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { AccountingMasterModel } from '../models/accountingMaster.model';
import { ProgressiveModel } from '../models/accountingProgressive.model';
import { PowerModel } from '../models/accountingPower.model';
import { PowerHistoryModel } from '../models/accountingPowerHistory.model';
import { CashlessModel } from '../models/accountingCashless.model';
import { CashlessHistoryModel } from '../models/accountingCashlessHistory.model';
import { SecurityModel } from '../models/accountingSecurity.model';
import { SecurityHistoryModel } from '../models/accountingSecurityHistory.model';
import { BillModel } from '../models/accountingBill.model';
import { BillHistoryModel } from '../models/accountingBillHistory.model';
import { HopperModel } from '../models/accountingHopper.model';
import { HopperHistoryModel } from '../models/accountingHopperHistory.model';

@Injectable()
export class AccountingService {

    public saved: boolean;
    public error: boolean;

    constructor(private restService: RestService) {
    }

    // GET Master Meters params
    getMaster(service: string): Observable<AccountingMasterModel>{
        return this.restService.getAssets(service);
    }

    // GET Progressive Meters
    getProgressive(service: string): Observable<ProgressiveModel>{
        return this.restService.getAssets(service);
    }

    // GET Power params
    getPower(service: string): Observable<PowerModel>{
        return this.restService.getAssets(service);
    }

    // GET Power History
    getPowerHistory(service: string): Observable<PowerHistoryModel>{
        return this.restService.getAssets(service);
    }

    // GET Cashless
    getCashless(service: string): Observable<CashlessModel>{
        return this.restService.getAssets(service);
    }

    // GET Cashless History
    getCashlessHistory(service: string): Observable<CashlessHistoryModel>{
        return this.restService.getAssets(service);
    }

    // GET Cashless
    getSecurity(service: string): Observable<SecurityModel>{
        return this.restService.getAssets(service);
    }

    // GET Security History
    getSecurityHistory(service: string): Observable<SecurityHistoryModel>{
        return this.restService.getAssets(service);
    }

    // GET Bill Acceptor
    getBillAcceptor(service: string): Observable<BillModel>{
        return this.restService.getAssets(service);
    }

    // GET Bill Acceptor History
    getBillHistory(service: string): Observable<BillHistoryModel>{
        return this.restService.getAssets(service);
    }

    // GET Hopper
    getHopper(service: string): Observable<HopperModel>{
        return this.restService.getAssets(service);
    }
    
    // GET HopperBill Acceptor History
    getHopperHistory(service: string): Observable<HopperHistoryModel>{
        return this.restService.getAssets(service);
    }
}
