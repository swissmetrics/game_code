import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';

@Injectable()
export class RestService {

    constructor(private http: Http, private httpClient: HttpClient) {
    }

    // GET method - API
    get(service: string): Observable<any> {
        return this.http
            .get(`${environment.apiServer}/${service}`)
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

    // GET method - local assets
    getAssets(service: string): Observable<any> {
        return this.httpClient.get(`${service}`);
    }

    // PUT method to save data - temporary method
    putAssets(service: string, body: Object): Observable<any> {
        return this.httpClient.get(`${service}`);
    }

    // PUT method - update
    put(service: string, body: Object): Observable<any> {
        let bodyString = JSON.stringify(body);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http
            .put(`${service}`, body, options)
            .map((response: Response) => response.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Connection error.'));
    }

}
