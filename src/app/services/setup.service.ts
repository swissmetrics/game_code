import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { LimitModel } from '../models/limit.model';
import { AcceptorModel } from '../models/acceptor.model';
import { HopperModel } from '../models/hopper.model';
import { ParameterModel } from '../models/parameter.model';
import { PrinterModel } from '../models/printer.model';
import { TicketModel } from '../models/ticket.model';

@Injectable()
export class SetupService {

    public saved: boolean;
    public error: boolean;

    constructor(private restService: RestService) {
    }

    // GET Limit setup
    getLimit(service: string): Observable<LimitModel>{
        return this.restService.getAssets(service);
    }

    // GET Acceptor setup
    getAcceptor(service: string): Observable<AcceptorModel>{
        return this.restService.getAssets(service);
    }

    // GET Hopper setup
    getHopper(service: string): Observable<HopperModel>{
        return this.restService.getAssets(service);
    }

    // GET Parameter setup
    getParameter(service: string): Observable<ParameterModel>{
        return this.restService.getAssets(service);
    }

    // GET Printer setup
    getPrinter(service: string): Observable<PrinterModel>{
        return this.restService.getAssets(service);
    }

    // GET Ticket setup
    getTicket(service: string): Observable<TicketModel>{
        return this.restService.getAssets(service);
    }

    // PUT Limit setup
    putLimit(service: string, body: Object): Observable<LimitModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Acceptor setup
    putAcceptor(service: string, body: Object): Observable<AcceptorModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Hopper setup
    putHopper(service: string, body: Object): Observable<HopperModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Parameter setup
    putParameter(service: string, body: Object): Observable<ParameterModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Printer setup
    putPrinter(service: string, body: Object): Observable<PrinterModel>{
        return this.restService.putAssets(service, body);
    }

    // PUT Ticket setup
    putTicket(service: string, body: Object): Observable<TicketModel>{
        return this.restService.putAssets(service, body);
    }

}
