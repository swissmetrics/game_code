import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { GamesModel } from '../models/games.model';
import { GameHistoryModel } from '../models/gameHistory.model';
import { GameParamsModel } from '../models/gameParams.model';
import { GameSetupModel } from '../models/gameSetup.model';

@Injectable()
export class GameService {

    public saved: boolean;
    public error: boolean;
    
    constructor(private restService: RestService) {
    }

    // GET all games
    getGames(service: string): Observable<GamesModel[]>{
        return this.restService.getAssets(service);
    }

    // GET game history
    getGameHistory(service: string): Observable<GameHistoryModel>{
        return this.restService.getAssets(service);
    }

    // GET game history params
    getGameParams(service: string): Observable<GameParamsModel>{
        return this.restService.getAssets(service);
    }

    // GET game settings
    getGameSetup(service: string): Observable<GameSetupModel>{
        return this.restService.getAssets(service);
    }

    // save game setting
    saveGameSetup(service: string, body: Object): Observable<GameSetupModel>{
        return this.restService.putAssets(service, body);
    }
}
