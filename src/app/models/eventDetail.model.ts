export interface EventDetailModel {
    id: number;
    type: string;
    source: string;
    results: number;
}
