export interface LimitModel{
    limits: {
        credit_in: {
            min: number,
            current: number,
            max: number
        },
        credit_limit: {
            min: number,
            current: number,
            max: number
        },
        jackpot: {
            min: number,
            current: number,
            max: number
        },
        celebration: {
            min: number,
            current: number,
            max: number
        },
        double: {
            min: number,
            current: number,
            max: number
        },
        hopper: {
            min: number,
            current: number,
            max: number
        },
        ticket_payout: {
            min: number,
            current: number,
            max: number
        },
        coins_in: {
            min: number,
            current: number,
            max: number
        },
        minimum_handpay: {
            min: number,
            current: number,
            max: number
        },
        even_handpay: {
            min: number,
            current: number,
            max: number
        }
    };
}
