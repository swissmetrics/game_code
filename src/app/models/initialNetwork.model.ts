export interface NetworkModel{
    settings: {
        protocol: {
            value: string,
            options: string[]
        },
        address: {
            value: number,
            type?: number | string
        },
        control: {
            value: "on" | "off",
            options: string[]
        },
        cashless: {
            value: "on"|"off",
            options: string[]
        },
        bonusing: {
            value: "on" | "off",
            options: string[]
        },
        ticket_validation: {
            value: "on" | "off",
            options: string[]
        },
        ticket_redemption: {
            value: "on" | "off",
            options: string[]
        },
        progressive_jackpot: {
            value: "on" | "off",
            options: string[]
        },
        mystery_jackpot: {
            value: "on" | "off",
            options: string[]
        },
        link_type: {
            value: string,
            options: string[]
        },
        jackpot_protocol: {
            value: string,
            options: string[]
        },
        jackpot_address: {
            value: number,
            type?: number | string
        },
        progressive_game: {
            value: string,
            options: string[]
        },
        show_win: {
            value: number,
            options: number[]
        },
        jackpots_levels: {lavel: number, value: "on" | "off", options: string[]}[]
    }
}
