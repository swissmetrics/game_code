export interface GeneralModel{
    settings: {
        currency: {
            value: string,
            options: string[],
        },
        main_coin: {
            value: number,
            options: number[]
        },
        tokenistaion: {
            value: number,
            options: number[]
        },
        la_win_limit: {
            value: number,
            type?: number | string
        },
        alarm_sound: {
            value: number,
            options: number[]
        },
        machine_number: {
            value: string,
            type?: number|string
        },
        validation_type: {
            value: number,
            options: number[]
        }
    };
}
