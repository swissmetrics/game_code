export interface ParameterModel{
    parameters: {
        start_mode: number,
        remote_input: number,
        handpay_credit: number,
        bill_input: number,
        coin_input: number,
        win_output: number,
        pay_jackpot: number,
        pay_credit: number,
        partial: number,
        hopper_reminder: number,
        hoppper_dump: number,
        hopper_strategy: number,
        wincounter: number,
        bill_acceptor: number,
        bonusing: number,
        ticket_redemption: number,
        language: string
    };
}
