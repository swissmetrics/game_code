export interface ConfigModel {
    id: string;
    language: string;
    is_configured: boolean;
    user: 'attendant' | 'operator'
}
