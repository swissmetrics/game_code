export interface ProgressiveModel {
    currency: string;
    levels: {
        level: number,
        name: string,
        startup: number,
        ceiling: number,
        current: number,
        overflow: number,
        hits: number,
        wins: number,
        increment: string,
        hidden_increment: string,
        initial_value: number
    }[];
}
