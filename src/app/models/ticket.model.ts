export interface TicketModel{
    info: {
        validation: string,
        barcode: string
    };
    setup: {
        credit_types: string,
        change_ticket: string,
        layout: number,
        language: string,
        date_format: string,
        handapy: string,
        ticket: string
    };
}
