export interface PowerModel {
    params: {
        last_fail: string,
        last_on: string,
        fail_count: number,
        games: number
    }
}
