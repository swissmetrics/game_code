export interface CashlessModel {
    currency: string;
    totals: {
        remoute_in: number,
        remoute_out: number,
        card_in: number,
        ticket_in: number,
        card_out: number,
        ticket_out: number,
        total_in_out: number,
        noncashable_in: number,
        promotional_in: number,
        noncashable_out: number,
        promotional_out: number
    };
    printer: {
        cashable_in: number,
        noncashable_in: number,
        promotional_in: number,
        cashable_out: number,
        noncashable_out: number,
        promotional_out: number,
        accepted: number,
        printed: number
    };
    card: {
        cashable_in: number,
        noncashable_in: number,
        promotional_in: number,
        cashable_out: number,
        noncashable_out: number,
        promotional_out: number,
        accepted: number
    };
    handpay: {
        total: number
    };
}
