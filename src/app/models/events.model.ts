export interface EventsModel {
    count: number;
    actions: {
        id: number,
        json: {
            name: string,
            payload: number,
            state: any,
            timestamp: number
        },
        name: string,
        type: any,
        updated_at: string
    }[];
}
