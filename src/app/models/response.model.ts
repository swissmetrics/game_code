export class ResponseModel {
    id: string;
    userId?: string;
    title?: string;
    body?: string;
}
