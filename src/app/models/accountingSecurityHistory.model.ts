export interface SecurityHistoryModel {
    count: number;
    history: {
        date: string,
        door: string,
        event: string
    }[];
}
