export interface ClockModel{
    settings: {
        utc: {
            value: string,
            options: string[]
        },
        local_time: {
            value: string,
            type?: number | string
        },
        time_zone: {
            value: number,
            options: number[]
        },
        summertime: {
            value: number,
            options: number[]
        }
    };
}
