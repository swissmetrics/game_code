export interface HopperHistoryModel {
    count: number;
    history: {
        date: string,
        channel: string,
        status: string
    }[];
}
