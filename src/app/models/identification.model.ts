export interface IdentificationModel {
    general: {
        date: string,
        currency: string,
        machine_model: string,
        serial_number: string,
        display_format: string,
        enable_residual_credit: string,
        enable_reserve: string,
        handpay_win: number,
        enable_handpay_reset: string,
        handpay_cr: string,
        celebration_win: string,
        credit_limit: string,
        cr_limit_win: string
    };
    peripherals: {
        touchscreen: string,
        bill_validator: string,
        coin_in_validator: string,
        credit_limit: string,
        printer: string,
        tower_light: string
    };
    progressive: {
        total: number,
        lp_levels: number,
        sap_levels: number,
        sap_rtp: string,
        group_id: number,
        handpay: number
    };
    sas: {
        enabled: string,
        cashless_transfer: string,
        asset_number: number,
        partial_transfer: string,
        bonusing: string,
        print_handpay: string,
        exception: string
    };
    xseries: {
        machine_number: string,
        version_number: string,
        manufacturer: string,
        firmware: string,
        credit_value: string,
        levels_supported: string,
        transfer_limit: number,
        hopper_limit: number,
        multigame: string,
        rtp: string
    };
    games: {
        id: number,
        name: string,
        theoretical_rtp: number,
        real_rtp: number,
        bet_per_game: number,
        total_in: number,
        total_out: number,
        image: string
    }[];
}
