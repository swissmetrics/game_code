export interface HandpayModel{
    settings: {
        current: {
            value: number,
            credits: number
        },
        cashable: {
            coins: number,
            validator: number,
            tito: number,
            other: number,
            remote: number
        },
        noncashable: {
            tito: number,
            tito_promotional: number,
            tito_noncashable: number,
            remote_promotional: number,
            remote_noncashable: number
        }
    }
}
