export interface HopperModel {
    currency: string;
    totals: {
        hopper: number,
        refilled: number,
        dumped: number,
        level: number
    };
    channels: {
        channel: string,
        value: number,
        count: number,
        credit: number,
        sum: number
    }[];
}
