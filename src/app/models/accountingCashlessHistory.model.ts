export interface CashlessHistoryModel {
    count: number;
    history: {
        date: string,
        channel: string,
        status: string,
        value: string
    }[];
}
