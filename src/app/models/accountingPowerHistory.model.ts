export interface PowerHistoryModel {
    count: number;
    history: {
        date: string,
        status: string
    }[];
}
