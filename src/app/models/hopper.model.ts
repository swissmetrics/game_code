export interface HopperModel{
    id: string;
    enabled: string;
    channels: {
        no: number,
        currency: string,
        value: number,
        full_level: {min: number, current: number, max: number},
        low_level: {min: number, current: number, max: number},
        refill_level: {min: number, current: number, max: number}
    }[]
}
