export interface AcceptorModel {
    id: string;
    enabled: string;
    channels: Array<{no: number, value: string, currency: string, enabled: string}>;
    directions?: Array<string>;
}
