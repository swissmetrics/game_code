export interface BillHistoryModel {
    count: number;
    history: {
        date: string,
        channel: string,
        status: string
    }[];
}
