export interface AccountingMasterModel {
    corrency: string;
    params: {
        general: {
            turnover: number,
            wins: number,
            cash: number,
            cancelled: number,
            money_in: number,
            money_out:number,
            games: number
        },
        coins: {
            in: number,
            out: number,
            extra: number
        },
        banknotes: {
            in: number,
            money_in: number,
            counts: number,
            rejects: number,
            banknote: {
                1: number,
                2: number,
                5: number,
                10: number,
                20: number,
                50: number,
                100: number
            }
        },
        cashless: {
            in: number,
            out: number
        },
        percentage: {
            rtp: number,
            calculated: number
        }
    };
}
