export interface BillModel{
    settings: {
        id: {
            value: string,
            type?: number | string
        },
        channels: {
            channel: number,
            enabled: 'on' | 'off',
            bill: number,
            currency: string,
            value?: number,
            options: number[]
        }[]
    }
}
