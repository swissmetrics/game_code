export interface HopperModel{
    settings: {
        id: {
            value: string,
            type?: number | string
        },
        channels: {
            channel: number,
            enabled: 'on' | 'off',
            coin: number,
            currency: string,
            value?: number,
            options: number[]
        }[]

    }
}
