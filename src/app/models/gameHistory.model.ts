export interface GameHistoryModel {
    game: {
        id: number,
        name: string
    };
    history: {
        id: number,
        time: string,
        in: number,
        out: number,
        feature: string,
        result: string
    }[];
    count: number;
}
