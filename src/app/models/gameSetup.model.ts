export interface GameSetupModel {
    game_id: number,
    game_name: string,
    params: [
        {
            name: string,
            value: number|string,
            type: string
        }
    ]
}
