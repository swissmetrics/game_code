export interface SecurityTypesModel {
    status: string;
    open: string;
    close: string;
    count: number;
    games: number;
}

export interface SecurityModel {
    main: SecurityTypesModel;
    belly: SecurityTypesModel;
    top: SecurityTypesModel;
    cashbox: SecurityTypesModel;
    bv: SecurityTypesModel;
}
