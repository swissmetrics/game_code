export interface GameParamsModel {
    id: number;
    next_id?: number,
    prev_id?: number,
    game: {
        id: number,
        name: string
    };
    image: string;
    params: {
        details: {
            time: string,
            bet: string,
            result: string,
            spin: string,
            gambles: string,
            gamble_value: string,
            gamble_out_value: string
        },
        last: {
            tokens_in: string,
            coins_in: string,
            bills_in: string,
            remote_in: string,
            card_in: string,
            bonus_in: string,
            ticket_in: string
        },
        winning_rules: {
            selected_symbol: string,
            winning_symbol: string
        }
    };
}
