export interface DumpModel {
    settings: {
        currency: string,
        levels: [
            {
                ratio: number,
                total: number,
                periodic: number
            }
        ]
    }
}
