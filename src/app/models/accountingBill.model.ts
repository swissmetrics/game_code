export interface BillModel {
    currency: string;
    rate: number;
    channels: {
        channel: string,
        value: number,
        count: number,
        credit: number,
        sum: number
    }[];
}
