export interface RefillModel {
    settings: {
        currency: string,
        levels: [
            {
                ratio: number,
                total: number,
                periodic: number,
                refill_level: number,
                missing: number
            }
        ]
    }
}
