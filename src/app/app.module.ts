import { BrowserModule }                from '@angular/platform-browser';
import { NgModule }                     from '@angular/core';
import { FormsModule }                  from '@angular/forms';
import { RouterModule }                 from '@angular/router';
import { HttpModule }                   from '@angular/http';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule }      from '@angular/platform-browser/animations';
import { APP_BASE_HREF }                from '@angular/common';
import { MaterialModule,
         MdNativeDateModule }           from '@angular/material';
import { environment }                  from '../environments/environment';

import { AppComponent }                 from './app.component';
import { HomeComponent }                from './components/home.component';
import { ModalComponent }               from './components/modal.component';
import { PaginationComponent }          from './components/pagination.component';
import { TimeFiltersComponent }         from './components/timeFilters.component';
import { DatapickerComponent }          from './components/datapicker.component';
import { KeyboardComponent }            from './components/keyboard.component';
import { IdentificationComponent }      from './components/identification.component';
import { AccountingComponent }          from './components/accounting/home.component';
import { AccountingMetersComponent }    from './components/accounting/meters.component';
import { AccountingAcceptorComponent }  from './components/accounting/acceptor.component';
import { AccountingSecurityComponent }  from './components/accounting/security.component';
import { AccountingHopperComponent }    from './components/accounting/hopper.component';
import { AccountingPowerComponent }     from './components/accounting/power.component';
import { AccountingCashlessComponent }  from './components/accounting/cashless.component';
import { AccountingProgressiveComponent } from './components/accounting/progressive.component';
import { AccountingErrorComponent }     from './components/accounting/error.component';
import { AttendantComponent }           from './components/attendant/home.component';
import { AttendantHandpayComponent }    from './components/attendant/handpay.component';
import { AttendantDumpComponent }       from './components/attendant/dump.component';
import { AttendantRefillComponent }     from './components/attendant/refill.component';
import { LogsComponent }                from './components/events/logs.component';
import { ModalEventComponent }          from './components/events/modalEvent.component';
import { ModalGameComponent }           from './components/events/modalGame.component';
import { ModalSmallGameComponent }      from './components/events/modalSmallGame.component';
import { GamesHistoryComponent }        from './components/events/gamesHistory.component';
import { GameHistoryComponent }         from './components/events/gameHistory.component';
import { SetupHomeComponent }           from './components/setup/home.component';
import { SetupLimitComponent }          from './components/setup/limit.component';
import { SetupParameterComponent }      from './components/setup/parameter.component';
import { SetupCoinComponent }           from './components/setup/coin.component';
import { SetupBillComponent }           from './components/setup/bill.component';
import { SetupHopperComponent }         from './components/setup/hopper.component';
import { SetupPrinterComponent }        from './components/setup/printer.component';
import { SetupTicketComponent }         from './components/setup/ticket.component';
import { SetupModalComponent }          from './components/setup/modal.component'
import { InitialHomeComponent }         from './components/initial/home.component';
import { InitialGeneralComponent }      from './components/initial/general.component';
import { InitialClockComponent }        from './components/initial/clock.component';
import { InitialNetworkComponent }      from './components/initial/network.component';
import { InitialCoinAcceptorComponent } from './components/initial/coin.component';
import { InitialBillAcceptorComponent } from './components/initial/bill.component';
import { InitialHopperComponent }       from './components/initial/hopper.component';
import { GamesComponent }               from './components/games/games.component';
import { GameComponent }                from './components/games/game.component';
import { TestComponent }                from './components/tests/test.component';
import { TestMenuComponent }            from './components/tests/menu.component';
import { TestSpinComponent }            from './components/tests/testSpin.component';
import { TestLedComponent }             from './components/tests/led.component';
import { TestTouchComponent }           from './components/tests/testTouch.component';
import { TestVideoComponent }           from './components/tests/testVideo.component';
import { TestAudioComponent }           from './components/tests/testAudio.component';
import { TestPrinterComponent }         from './components/tests/testPrinter.component';
import { TestHopperComponent }          from './components/tests/testHopper.component';
import { TestBillComponent }            from './components/tests/testBill.component';
import { TestSelfComponent }            from './components/tests/testSelf.component';
import { SummaryTestsComponent }        from './components/tests/summary.component';
import { InputMoveDirective }           from './directives/inputmove.directive';
import { InputNumbersDirective }        from './directives/inputnumbers.directive';
import { NavigationDirective }          from './directives/navigation.directive';
import { KeyboardDirective }            from './directives/keyboard.directive';

import { RoutingModule }                from './routing.module';
import { ConfigService }                from './services/config.service';
import { AuthService }                  from './services/auth.service';
import { WebsocketService }             from './services/websocket.service';
import { TestsService }                 from './services/tests.service';
import { RestService }                  from './services/rest.service';
import { SetupService }                 from './services/setup.service';
import { InitialService }               from './services/initial.service';
import { GameService }                  from './services/game.service';
import { EventsService }                from './services/events.service';
import { IdentificationService }        from './services/identification.service';
import { NavigationService }            from './services/navigation.service';
import { KeyboardService }              from './services/keyboard.service';
import { MdKeyboardModule }             from 'ngx-material-keyboard';
import { TranslateHttpLoader }          from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import 'hammerjs';
import { KeysPipe }                     from './pipes/keys.pipe';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
      AppComponent, ModalComponent, PaginationComponent, KeysPipe, DatapickerComponent, HomeComponent, IdentificationComponent,
      LogsComponent, ModalEventComponent, ModalGameComponent, ModalSmallGameComponent, TimeFiltersComponent, KeyboardComponent,
      SetupHomeComponent, SetupLimitComponent, SetupParameterComponent, SetupCoinComponent, SetupBillComponent,
      SetupHopperComponent, SetupPrinterComponent, SetupTicketComponent, SetupModalComponent,
      InitialHomeComponent, InitialGeneralComponent, InitialClockComponent, InitialNetworkComponent,
      InitialCoinAcceptorComponent, InitialBillAcceptorComponent, InitialHopperComponent,
      AccountingComponent, AccountingMetersComponent, AccountingAcceptorComponent, AccountingSecurityComponent,
      AccountingHopperComponent, AccountingPowerComponent, AccountingCashlessComponent, AccountingProgressiveComponent,
      AccountingErrorComponent, AttendantComponent, AttendantHandpayComponent, AttendantDumpComponent, AttendantRefillComponent,
      GamesComponent, GameComponent, GameHistoryComponent, GamesHistoryComponent,
      TestMenuComponent, TestComponent, TestSpinComponent, TestLedComponent, TestTouchComponent, TestHopperComponent,
      TestVideoComponent, TestAudioComponent, TestPrinterComponent, SummaryTestsComponent,
      TestBillComponent, TestSelfComponent, InputMoveDirective, InputNumbersDirective, NavigationDirective, KeyboardDirective
  ],
  imports: [
      BrowserModule, RoutingModule, HttpModule, FormsModule, MaterialModule, MdNativeDateModule,
      BrowserAnimationsModule, HttpClientModule, MdKeyboardModule,
      TranslateModule.forRoot({
          loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
      })
  ],
  providers: [
      ConfigService, AuthService, WebsocketService, SetupService, InitialService, GameService,
      EventsService, TestsService, RestService, NavigationService, KeyboardService, IdentificationService,
      {
          provide: APP_BASE_HREF,
          useValue: '/'
      }
  ],
  bootstrap: [AppComponent]
})

export class AppModule{}
