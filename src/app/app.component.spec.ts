import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ModalComponent } from './components/modal.component';
import { ConfigService } from './services/config.service';
import { AuthService } from './services/auth.service';
import { RestService } from './services/rest.service';
import { NavigationService } from './services/navigation.service';
import { WebsocketService } from './services/websocket.service';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';

describe('AppComponent', () => {

    let fixture: ComponentFixture<AppComponent>;
    let component: AppComponent;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            imports: [ RouterTestingModule, HttpModule, HttpClientModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, './assets/i18n/', '.json'),
                        deps: [HttpClient]
                }
                })
            ],
            declarations: [AppComponent, ModalComponent],
            providers: [
                ConfigService, AuthService, RestService, HttpClient,
                TranslateService, NavigationService, WebsocketService
            ],
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(AppComponent);
            component = fixture.componentInstance;
            component.ngOnInit();
        });
    }));

    it('should create the app', async(() => {
        fixture.whenStable().then(() => {
            const app = fixture.debugElement.componentInstance;
            expect(app).toBeTruthy();
        });
    }));

});
