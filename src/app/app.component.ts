import { Component, OnInit, ElementRef, Renderer } from '@angular/core';
import { ConfigService } from './services/config.service';
import { AuthService } from './services/auth.service';
import { NavigationService } from './services/navigation.service';
import { ConfigModel } from './models/config.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

    public title = "Attendant Menu";
    public config: ConfigModel;
    public tabs: {name: string, link: string}[];
    public showKeypad: boolean;

    constructor(
        private configService: ConfigService,
        private authService: AuthService,
        private navigationService: NavigationService,
        private translate: TranslateService) {
            translate.addLangs(['en', 'pl', 'ru']);
            translate.setDefaultLang('en');
    }

    ngOnInit() {

        let serviceApi = 'assets/jsons/config.json';
        this.configService
            .getConfig(serviceApi)
            .subscribe( ret => {
                this.config = ret;
                this.configService.config = ret;
                this.translate.use(ret.language);
            });

        this.menu();

        // listen to WS button commands
        this.navigationService.wsConnect();
    }

    // navigation by buttons
    // after tests delete the following methods and use WS in navigationService.
    // up button
    goMenu() {
        this.navigationService.goMenu();
    }

    // down (select) button
    goDown() {
        this.navigationService.goDown();
    }

    // right, left buttons
    nextTab(direction:number) {
        this.navigationService.nextTab(direction);
    }

    // if show/hide menu tab
    authMenuTab(segment: string) {
        return this.authService.checkUrl(segment);
    }

    menu() {
        this.tabs = [
            {name: 'identification', link: '/identification'},
            {name: 'attendant', link: '/attendant'},
            {name: 'accounting.main', link: '/accounting'},
            {name: 'logs', link: '/logs'},
            {name: 'game_history', link: '/game-history'},
            {name: 'testmode', link: '/tests'},
            {name: 'game', link: '/games'},
            {name: 'machine_setup', link: '/setup'},
            {name: 'initial_setup', link: '/initial'},
        ];
    }
}
