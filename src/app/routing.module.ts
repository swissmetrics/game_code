import { NgModule }                     from '@angular/core';
import { RouterModule, Routes }         from '@angular/router';
import { HomeComponent }                from './components/home.component';
import { IdentificationComponent }      from './components/identification.component';
import { AccountingComponent }          from './components/accounting/home.component';
import { AccountingMetersComponent }    from './components/accounting/meters.component';
import { AccountingAcceptorComponent }  from './components/accounting/acceptor.component';
import { AccountingSecurityComponent }  from './components/accounting/security.component';
import { AccountingHopperComponent }    from './components/accounting/hopper.component';
import { AccountingPowerComponent }     from './components/accounting/power.component';
import { AccountingCashlessComponent }  from './components/accounting/cashless.component';
import { AccountingProgressiveComponent } from './components/accounting/progressive.component';
import { AccountingErrorComponent }     from './components/accounting/error.component';
import { AttendantComponent }           from './components/attendant/home.component';
import { AttendantHandpayComponent }    from './components/attendant/handpay.component';
import { AttendantDumpComponent }       from './components/attendant/dump.component';
import { AttendantRefillComponent }     from './components/attendant/refill.component';
import { LogsComponent }                from './components/events/logs.component';
import { SetupHomeComponent }           from './components/setup/home.component';
import { SetupLimitComponent }          from './components/setup/limit.component';
import { SetupParameterComponent }      from './components/setup/parameter.component';
import { SetupCoinComponent }           from './components/setup/coin.component';
import { SetupBillComponent }           from './components/setup/bill.component';
import { SetupHopperComponent }         from './components/setup/hopper.component';
import { SetupPrinterComponent }        from './components/setup/printer.component';
import { SetupTicketComponent }         from './components/setup/ticket.component';
import { InitialHomeComponent }         from './components/initial/home.component';
import { InitialGeneralComponent }      from './components/initial/general.component';
import { InitialClockComponent }        from './components/initial/clock.component';
import { InitialNetworkComponent }      from './components/initial/network.component';
import { InitialCoinAcceptorComponent } from './components/initial/coin.component';
import { InitialBillAcceptorComponent } from './components/initial/bill.component';
import { InitialHopperComponent }       from './components/initial/hopper.component';
import { TestComponent }                from './components/tests/test.component';
import { GamesComponent }               from './components/games/games.component';
import { GameComponent }                from './components/games/game.component';
import { GamesHistoryComponent }        from './components/events/gamesHistory.component';
import { GameHistoryComponent }         from './components/events/gameHistory.component';
import { TestSpinComponent }            from './components/tests/testSpin.component';
import { TestLedComponent }             from './components/tests/led.component';
import { TestTouchComponent }           from './components/tests/testTouch.component';
import { TestVideoComponent }           from './components/tests/testVideo.component';
import { TestAudioComponent }           from './components/tests/testAudio.component';
import { TestPrinterComponent }         from './components/tests/testPrinter.component';
import { TestHopperComponent }          from './components/tests/testHopper.component';
import { TestBillComponent }            from './components/tests/testBill.component';
import { TestSelfComponent }            from './components/tests/testSelf.component';
import { SummaryTestsComponent }        from './components/tests/summary.component';
import { AuthService }                  from './services/auth.service';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'identification', component: IdentificationComponent },
  { path: 'logs', component: LogsComponent },
];

const childs: Routes = [
    { path: 'tests', children: [
        { path: '', component: TestComponent },
        { path: 'testing/test-1', component: TestSpinComponent },
        { path: 'testing/test-2', component: TestLedComponent },
        { path: 'testing/test-3', component: TestTouchComponent },
        { path: 'testing/test-4', component: TestVideoComponent },
        { path: 'testing/test-5', component: TestAudioComponent },
        { path: 'testing/test-6', component: TestPrinterComponent },
        { path: 'testing/test-7', component: TestHopperComponent },
        { path: 'testing/test-8', component: TestSelfComponent },
        { path: 'testing/test-9', component: TestBillComponent },
        { path: 'testing/summary', component: SummaryTestsComponent }
    ]},
    { path: 'games', canActivateChild: [AuthService], children: [
        { path: '', component: GamesComponent },
        { path: ':id', component: GameComponent }
    ]},
    { path: 'game-history', children: [
        { path: '', component: GamesHistoryComponent },
        { path: ':id', component: GameHistoryComponent }
    ]},
    { path: 'initial', canActivateChild: [AuthService], children: [
        { path: '', component: InitialHomeComponent },
        { path: 'general', component: InitialGeneralComponent },
        { path: 'clock', component: InitialClockComponent },
        { path: 'network', component: InitialNetworkComponent },
        { path: 'coin-acceptor', component: InitialCoinAcceptorComponent },
        { path: 'bill-acceptor', component: InitialBillAcceptorComponent },
        { path: 'hopper', component: InitialHopperComponent }
    ]},
    { path: 'setup', canActivateChild: [AuthService], children: [
        { path: '', component: SetupHomeComponent },
        { path: 'limit', component: SetupLimitComponent },
        { path: 'parameter', component: SetupParameterComponent },
        { path: 'coin', component: SetupCoinComponent },
        { path: 'bill', component: SetupBillComponent },
        { path: 'hopper', component: SetupHopperComponent },
        { path: 'printer', component: SetupPrinterComponent },
        { path: 'ticket', component: SetupTicketComponent }
    ]},
    { path: 'attendant', children: [
        { path: '', component: AttendantComponent },
        { path: 'handpay', component: AttendantHandpayComponent },
        { path: 'dump', component: AttendantDumpComponent },
        { path: 'refill', component: AttendantRefillComponent }
    ]},
    { path: 'accounting', children: [
        { path: '', component: AccountingComponent },
        { path: 'meters', component: AccountingMetersComponent },
        { path: 'acceptor', component: AccountingAcceptorComponent },
        { path: 'security', component: AccountingSecurityComponent },
        { path: 'hopper', component: AccountingHopperComponent },
        { path: 'power', component: AccountingPowerComponent },
        { path: 'cashless', component: AccountingCashlessComponent },
        { path: 'progressive', component: AccountingProgressiveComponent },
        { path: 'error', component: AccountingErrorComponent }
    ]}
];

@NgModule({
    // RouterModule.forRoot(routes, { useHash: true }),
  imports: [ RouterModule.forRoot(routes, { useHash: true }), RouterModule.forChild(childs) ],
  exports: [ RouterModule ],
  providers: [ AuthService ]
})

export class RoutingModule {}
