import { Directive, HostListener, ElementRef, Input}  from '@angular/core';

@Directive({
  selector: '[inputNumbers]'
})

export class InputNumbersDirective {

    // type = [int|float]
    @Input('inputNumbers') type = ['int'];

    private el: HTMLInputElement;

    constructor(private elementRef: ElementRef){
        this.el = this.elementRef.nativeElement;
    }

    @HostListener('focus', ['$event'])
    onFocus(){

    }

    @HostListener('blur', ['$event.target.value'])
    onBlur(value) {
        this.el.value = value;
    }


}
