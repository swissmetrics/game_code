import { Directive, HostListener, ElementRef, Renderer2 }  from '@angular/core';
import { KeyboardService } from '../services/keyboard.service';

@Directive({
    selector: '[keyboardNumbers]'
})

// numeric keyboard
export class KeyboardDirective {

    private el: HTMLInputElement;

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2,
        private keyboardService: KeyboardService) {
    }

    @HostListener('click', ['$event'])
    onClick(event) {
        this.keyboardService.pressedKey = '';
        this.keyboardService.currentValue = '';
        this.keyboardService.eventTarget = event.target;
        this.keyboardService.orginalValue = event.target.value;
        event.target.value = '';
        let inputs = document.querySelectorAll('input[keyboardnumbers]');
        for(let input of Array.from(inputs)) {
            this.renderer.removeClass(input, 'active')
        }
        this.renderer.addClass(this.elementRef.nativeElement, 'active');
    }

    @HostListener('focus', ['$event'])
    onFocus(event) {
        this.keyboardService.showKeyboard = true;
        this.renderer.addClass(this.elementRef.nativeElement, 'active');
    }

    @HostListener('blur', ['$event'])
    onBlur(event) {
        setTimeout( () => {
            let activeElement = this.elementRef.nativeElement.ownerDocument.activeElement;
            if(!activeElement.hasAttribute('data-key') && !activeElement.hasAttribute('keyboardNumbers')) {
                this.keyboardService.showKeyboard = false;
                this.renderer.removeClass(this.elementRef.nativeElement, 'active');
            }
            // detect change by ngModel
            // let customEvent = document.createEvent('Event');
            // customEvent.initEvent('input', true, true);
            // this.elementRef.nativeElement.dispatchEvent(customEvent);
        }, 100)
        if(!event.target.value) {
            event.target.value = this.keyboardService.orginalValue;
        }
    }
}
