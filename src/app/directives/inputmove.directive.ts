import { Directive, HostListener, ElementRef }  from '@angular/core';

@Directive({
  selector: '[inputMove]'
})

export class InputMoveDirective {

    private keyboardHeight: number = 300;
    private pos: number = 0;
    private overKeyboard: number = 0;
    private parrentElement: any;

    constructor(private el: ElementRef){
    }

    @HostListener('focus', ['$event'])
    onFocus(){

        this.parrentElement = this.el.nativeElement.parentNode.parentNode.parentNode.parentNode;

        this.pos = this.parrentElement.offsetTop;
        this.overKeyboard = window.innerHeight - this.keyboardHeight;

        let movement = (window.innerHeight - this.pos) + (this.pos - this.overKeyboard);

        if( this.pos > this.overKeyboard){
            setTimeout(() => {
                this.parrentElement.style.marginBottom = `${movement}px`;
                this.parrentElement.scrollIntoView({
                  behavior: 'smooth'
                });
            }, 350);
        }

    }

    @HostListener('focusout', ['$event'])
    focusOut(){
        if( this.pos > this.overKeyboard){
            this.parrentElement.style.marginBottom = `0px`;
            this.parrentElement.scrollIntoView(true);
        }
    }
}
