import { Directive, HostListener, ElementRef, Input }  from '@angular/core';
import {NavigationService} from '../services/navigation.service';

@Directive({
    selector: '[tabIndex]'
})

// get focus on elements which are available in buttons navigation
export class NavigationDirective {

    // instructions = [order, ifClicked]
    @Input('tabIndex') instructions = [];

    constructor(private el: ElementRef, private navigationService: NavigationService){
    }

    @HostListener('focus', ['$event'])
    onFocus(){

        this.navigationService.tabIndex = this.instructions[0];
        this.navigationService.tabAction = this.instructions[1];
        this.navigationService.tabType = this.instructions[2];

        if(this.instructions[1] === 'click'){
            this.el.nativeElement.click();
        }
        else if(this.instructions[1] === 'select'){
            this.el.nativeElement.size = 3;
        }
    }

    @HostListener('focusout', ['$event'])
    onFocusout() {
        if(this.instructions[1] === 'select'){
            this.el.nativeElement.size = 0;
        }
    }

}
