export const environment = {
  production: true,
  wsServer: '/socket',
  apiServer: '/api'
};
