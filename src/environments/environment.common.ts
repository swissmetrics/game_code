let query = new URLSearchParams(location.search)
let server = query.get('serverName');
let port = query.get('serverPort');
let path = location.pathname.length === 1 ? '' : location.pathname;

export const envCommon = {
  serverName: (server ? server : location.hostname) + ':' + (port ? port : '4000'),
  serverPath: path
};
