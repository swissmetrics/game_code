## How to run Attendantmenu ##

### From docker ###

``` bash
docker run --net=host -it gamecode/backend
docker run --net=host -it gamecode/attendant-menu
```

### From command line ###

``` bash
mix do deps.get, \
       ecto.create, \
       ecto.migrate, \
       build
mix start
```

url: http://localhost:4000/attendant_menu/index.html

OR

```
cd attendant_menu
npm install
ng serve
```

### Production mode ###

To run on the production use:

```ng build --env=prod```


### Backend URLs for API & WS ###

- Default backend host name is the same as client host name (i.e. localhost)
- Default port is 4000.
To change above parameters, please use get arguments in the url.
```&serverName=...&serverPort=...```

Example:

```http://am.gamecodehq.com/?serverName=localhost&serverPort=3333```

This calls the following backend addresses:
 - ws://localhost:3333/socket
 - http://localhost:3333/api



## To display events from the DB ##
First you need to add events to Postgres:

```
iex -S mix
```

```
State.dispatch %{name: "test_add", type: "spin", payload: 10}
State.dispatch %{name: "test_add", type: "press", payload: 20}
etc
```

## Translations
All translation files are in /assets/i18n/ folder.

## Settings
Each field should have type: string, select, checkbox.
Please look at assets/jsons/machines-1.json file.

## Game setup
There are mockup files for this one:
- assets/jsons/games.json -> available games
- assets/jsons/games-1.json - > game configuration

## Test mode ##
Tests are running one by one after pressing button RUN ALL.
When some test fails the testing process is stopped.
You can select a particular test by clicking the button in the right menu.
There are three states for each test that are marked by other color.
- in progress -> orange
- passed -> green
- failed -> red

### Spin button ###
The RUN button appears when is established connection to the web socket.
And then after pressing the RUN button is tested communication through web sockets - the application waits for server response on the "xxx" channel.
After the test is over the connection is disconnected.

### Touch screen ###
There are displayed points with random coordinates.
You should touch each of them.
The test is passed when all points are selected.

## Events ##
Now are fake .json files in /assets/jsons/*
Replace "serviceApi" variable for url to the REST service.
