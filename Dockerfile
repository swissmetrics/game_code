FROM ubuntu:16.04

RUN echo "Install additional packages ..." \
  && apt-get update \
  && apt-get install -q -y locales wget build-essential git \
  && locale-gen en_US.UTF-8

RUN echo "Installing node ..." \
  && wget -O - https://deb.nodesource.com/setup_8.x | bash - \
  && apt-get install -q -y nodejs

COPY . src

WORKDIR src

RUN echo "Building..." \
  && cd src \
  && npm install \
  && npm run dist

CMD cd src && npm start
