import { MenuPage } from './app.po';

describe('menu App', () => {
  let page: MenuPage;

  beforeEach(() => {
    page = new MenuPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
